﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clinica.Model
{
    public class Especialidade
    {
        public Int32 id { get; set; }
        public String titulo { get; set; }
        public String descricao { get; set; }

        public Especialidade(Int32 id, String titulo, String descricao)
        {
            this.id = id;
            this.titulo = titulo;
            this.descricao = descricao;
        }
    }
}