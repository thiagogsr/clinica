﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clinica.Model
{
    public class Medico
    {
        public Int32 id { get; set; }
        public String nome { get; set; }
        public String endereco { get; set; }
        public String cidade { get; set; }
        public String estado { get; set; }
        public String cpf { get; set; }
        public String telefone { get; set; }
        public String crm { get; set; }
        public int atendimentos { get; set; }
        public Clinica.Model.Especialidade especialidade { get; set; }

        public Medico(String nome, String endereco, String cidade, String estado, String cpf, String telefone, String crm, int atendimentos, Clinica.Model.Especialidade especialidade)
        {
            this.nome = nome;
            this.endereco = endereco;
            this.cidade = cidade;
            this.estado = estado;
            this.cpf = cpf;
            this.telefone = telefone;
            this.crm = crm;
            this.atendimentos = atendimentos;
            this.especialidade = especialidade;
        }
    }
}