using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clinica.Model
{
    public class Exame
    {
        public Int32 id { get; set; }
        public String titulo { get; set; }
        public String codigo { get; set; }
        public String observacoes { get; set; }
        public List<Clinica.Model.Convenio> convenios { get; set; }

        public Exame(Int32 id, String titulo, String codigo, String observacoes, List<Clinica.Model.Convenio> convenios)
        {
            this.id = id;
            this.titulo = titulo;
            this.codigo = codigo;
            this.observacoes = observacoes;
            this.convenios = convenios;
        }
    }
}