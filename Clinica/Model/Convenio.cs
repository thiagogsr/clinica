﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clinica.Model
{
    public class Convenio
    {
        public Int32 id { get; set; }
        public String titulo { get; set; }
        public String sigla { get; set; }
        public String telefone { get; set; }

        public Convenio(Int32 id, String titulo, String sigla, String telefone)
        {
            this.id = id;
            this.titulo = titulo;
            this.sigla = sigla;
            this.telefone = telefone;
        }
    }
}