using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clinica.Model
{
    public class RequisicaoExame
    {
        public Int32 id { get; set; }
        public Paciente paciente { get; set; }
        public Exame exame { get; set; }
        public DateTime data { get; set; }
        public String tipo { get; set; }
        public Decimal valor { get; set; }

        public RequisicaoExame(Int32 id, Paciente paciente, Exame exame, DateTime data, String tipo, Decimal valor)
        {
            this.id = id;
            this.paciente = paciente;
            this.exame = exame;
            this.data = data;
            this.tipo = tipo;
            this.valor = valor;
        }
    }
}