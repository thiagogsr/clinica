﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clinica.Model
{
    public class Consulta
    {
        public Int32 id { get; set; }
        public Paciente paciente { get; set; }
        public Convenio convenio { get; set; }
        public Medico medico { get; set; }
        public DateTime data { get; set; }
        public String turno { get; set; }
        public String situacao { get; set; }
        public String medicamentos { get; set; }

        public Consulta(Int32 id, Paciente paciente, Convenio convenio, Medico medico, DateTime data, String turno, String situacao, String medicamentos)
        {
            this.id = id;
            this.paciente = paciente;
            this.convenio = convenio;
            this.medico = medico;
            this.data = data;
            this.turno = turno;
            this.situacao = situacao;
            this.medicamentos = medicamentos;
        }
    }
}