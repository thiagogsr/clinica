﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clinica.Model
{
    public class Paciente
    {
        public Int32 id { get; set; }
        public String nome { get; set; }
        public String endereco { get; set; }
        public String cidade { get; set; }
        public String estado { get; set; }
        public String cpf { get; set; }
        public String telefone { get; set; }
        public Nullable<DateTime> aniversario { get; set; }
        public String sexo { get; set; }
        public Convenio convenio { get; set; }

        public Paciente(String nome, String endereco, String cidade, String estado, String cpf, String telefone, Nullable<DateTime> aniversario, String sexo, Convenio convenio)
        {
            this.nome = nome;
            this.endereco = endereco;
            this.cidade = cidade;
            this.estado = estado;
            this.cpf = cpf;
            this.telefone = telefone;
            this.aniversario = aniversario;
            this.sexo = sexo;
            this.convenio = convenio;
        }
    }
}