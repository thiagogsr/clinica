﻿$(document).ready(function () {
    $('.moeda').priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: ''
    });

    var tipoConvenio = $('#TipoConvenio'),
        tipoParticular = $('#TipoParticular'),
        showParticular = $('.show-particular');

    function verificarTipo() {
        if (tipoConvenio.is(':checked')) {
            showParticular.hide();
        }

        if (tipoParticular.is(':checked')) {
            showParticular.show();
        }
    }

    tipoConvenio.on('click', function () {
        verificarTipo();
    });

    tipoParticular.on('click', function () {
        verificarTipo();
    });

    verificarTipo();
});