﻿$(document).ready(function () {
    var situacao = $('[name=Situacao]'),
        showRealizada = $('.show-realizada');

    function verificarSituacao() {
        if (situacao.filter(':checked').val() == 'REALIZADA') {
            showRealizada.show();
        } else {
            showRealizada.hide();
        }
    }

    situacao.on('click', function () {
        verificarSituacao();
    });

    verificarSituacao();
});