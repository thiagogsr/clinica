using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Clinica.Helpers
{
    public class Exame
    {
        public static void popular(DropDownList element)
        {
            if (element.Items.Count == 1)
            {
                Clinica.Controllers.Exame exames_controller = Clinica.Controllers.Exame.getInstance();
                element.DataSource = exames_controller.exames;
                element.AppendDataBoundItems = true;
                element.DataTextField = "titulo";
                element.DataValueField = "id";
                element.DataBind();
            }
        }

        public static Clinica.Model.Exame search(String exame_selected)
        {
            Clinica.Model.Exame exame = null;
            if (exame_selected.Trim() != "")
            {
                Int32 exame_id = Int32.Parse(exame_selected);
                exame = Clinica.Controllers.Exame.getInstance().search(exame_id);
            }
            return exame;
        }
    }
}