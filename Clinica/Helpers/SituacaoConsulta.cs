﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Clinica.Helpers
{
    public class SituacaoConsulta
    {
        public static void popular(RadioButtonList element)
        {
            Dictionary<String, String> situacoes = new Dictionary<String, String>();
            situacoes.Add("AGENDADA", "Agendada");
            situacoes.Add("REALIZADA", "Realizada");
            situacoes.Add("CANCELADA", "Cancelada");
            situacoes.Add("RETORNO", "Retorno");

            element.DataSource = situacoes;
            element.DataTextField = "Value";
            element.DataValueField = "Key";
            element.DataBind();
            element.RepeatLayout = RepeatLayout.UnorderedList;
        }
    }
}