﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Clinica.Helpers
{
    public class Util
    {
        public static String onlyNumbers(String text)
        {
            return Regex.Replace(text, @"\D+", "");
        }

        public static DateTime parseDate(String data)
        {
            return DateTime.Parse(data);
        }

        public static Decimal parseDecimal(String valor)
        {
            return Decimal.Parse(valor);
        }
    }
}