﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Clinica.Helpers
{
    public class Paciente
    {
        public static void popular(DropDownList element)
        {
            if (element.Items.Count == 1)
            {
                element.DataSource = Clinica.Controllers.Paciente.getInstance().pacientes;
                element.AppendDataBoundItems = true;
                element.DataTextField = "nome";
                element.DataValueField = "cpf";
                element.DataBind();
            }
        }

        public static Clinica.Model.Paciente search(String paciente_selected)
        {
            Clinica.Model.Paciente paciente = null;
            if (paciente_selected.Trim() != "")
            {
                paciente = Clinica.Controllers.Paciente.getInstance().search(paciente_selected);
            }
            return paciente;
        }
    }
}