﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Clinica.Helpers
{
    public class Especialidade
    {
        public static void popular(DropDownList element)
        {
            if (element.Items.Count == 1)
            {
                Clinica.Controllers.Especialidade especialidades_controller = Clinica.Controllers.Especialidade.getInstance();
                element.DataSource = especialidades_controller.especialidades;
                element.AppendDataBoundItems = true;
                element.DataTextField = "titulo";
                element.DataValueField = "id";
                element.DataBind();
            }
        }

        public static Clinica.Model.Especialidade search(String especialidade_selected)
        {
            Clinica.Model.Especialidade especialidade = null;
            if (especialidade_selected.Trim() != "")
            {
                Int32 especialidade_id = Int32.Parse(especialidade_selected);
                especialidade = Clinica.Controllers.Especialidade.getInstance().search(especialidade_id);
            }
            return especialidade;
        }
    }
}