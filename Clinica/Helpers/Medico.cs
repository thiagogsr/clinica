﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Clinica.Helpers
{
    public class Medico
    {
        public static void popular(DropDownList element)
        {
            if (element.Items.Count == 1)
            {
                element.DataSource = Clinica.Controllers.Medico.getInstance().medicos;
                element.AppendDataBoundItems = true;
                element.DataTextField = "nome";
                element.DataValueField = "crm";
                element.DataBind();
            }
        }

        public static Clinica.Model.Medico search(String medico_selected)
        {
            Clinica.Model.Medico medico = null;
            if (medico_selected.Trim() != "")
            {
                medico = Clinica.Controllers.Medico.getInstance().search(medico_selected);
            }
            return medico;
        }
    }
}