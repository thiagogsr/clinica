﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Clinica.Helpers
{
    public class Estado
    {
        public static void popular(DropDownList element)
        {
            if (element.Items.Count == 1)
            {
                Clinica.Controllers.Estado estados_controller = Clinica.Controllers.Estado.getInstance();
                element.DataSource = estados_controller.estados;
                element.AppendDataBoundItems = true;
                element.DataTextField = "Value";
                element.DataValueField = "Key";
                element.DataBind();
            }
        }
    }
}