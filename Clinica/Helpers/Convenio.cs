﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Clinica.Helpers
{
    public class Convenio
    {
        public static void popular(DropDownList element)
        {
            if (element.Items.Count == 1)
            {
                element.DataSource = Clinica.Controllers.Convenio.getInstance().convenios;
                element.AppendDataBoundItems = true;
                element.DataTextField = "titulo";
                element.DataValueField = "id";
                element.DataBind();
            }
        }

        public static Clinica.Model.Convenio search(String convenio_selected)
        {
            Clinica.Model.Convenio convenio = null;
            if (convenio_selected.Trim() != "")
            {
                Int32 convenio_id = Int32.Parse(convenio_selected);
                convenio = Clinica.Controllers.Convenio.getInstance().search(convenio_id);
            }
            return convenio;
        }
    }
}