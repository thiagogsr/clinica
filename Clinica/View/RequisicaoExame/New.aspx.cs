﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clinica.View.RequisicaoExame
{
    public partial class New : System.Web.UI.Page
    {
        Clinica.Controllers.RequisicaoExame requisicao_exames_controller;

        protected void Page_Load(object sender, EventArgs e)
        {
            requisicao_exames_controller = Clinica.Controllers.RequisicaoExame.getInstance();

            if (!IsPostBack) {
                popularCombos();
            }
        }

        protected void Criar_Click(object sender, EventArgs e)
        {
            Boolean created = requisicao_exames_controller.create(Paciente.Text, Exame.Text, Data.Text, TipoConvenio.Checked, Valor.Text);
            if (created)
            {
                Session["flash_message"] = "Requisição de Exame cadastrada com sucesso";
                clearFields();
            }
            else
            {
                Session["flash_message"] = "O convênio do paciente não permite a realização deste exame.";
                popularCombos();
            }
        }

        private void popularCombos()
        {
            Clinica.Helpers.Paciente.popular(Paciente);
            Clinica.Helpers.Exame.popular(Exame);
        }

        private void clearFields()
        {
            Paciente.Text = "";
            Exame.Text = "";
            Data.Text = "";
            Valor.Text = "";
        }
    }
}