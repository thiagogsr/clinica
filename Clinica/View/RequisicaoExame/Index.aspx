﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Clinica.View.RequisicaoExame.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Requisição de Exames</title>
    <link href="../../Assets/Stylesheet/Reset.css" rel="stylesheet" />
    <link href="../../Assets/Stylesheet/App.css" rel="stylesheet" />
</head>
<body>
    <div class="wrap">
        <h2 class="title">Requisição de Exames</h2>
        <%
            if (Session["flash_message"] != "") {
                Response.Write("<p class='flash-message'>" + Session["flash_message"] + "</p>");
                Session.Remove("flash_message");
            }
        %>
        <a href="/" title="Início" class="btn-link">Início</a>
        <a href="New.aspx" title="Cadastrar Requisição de Exame" class="btn-link">Cadastrar Requisição de Exame</a>
        <form id="BuscarRequisicaoExame" method="get" runat="server">
            <div class="container">
                <div class="half">
                </div>
            </div>

            <asp:GridView ID="RequisicaoExamesList" runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None"
                BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Horizontal" Width="100%"
                OnRowDataBound="RequisicaoExamesList_RowDataBound" DataKeyNames="id">
                <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                <SortedDescendingHeaderStyle BackColor="#242121" />
                <Columns>
                    <asp:BoundField HeaderText="ID" DataField="id" />
                    <asp:BoundField HeaderText="Paciente" DataField="paciente.nome" />
                    <asp:BoundField HeaderText="Exame" DataField="exame.titulo" />
                    <asp:BoundField HeaderText="Data" DataField="data" />
                    <asp:BoundField HeaderText="Tipo" DataField="tipo" />
                    <asp:BoundField HeaderText="Valor" DataField="valor" />
                    <asp:HyperLinkField Text="Editar" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="Excluir" runat="server" Text="Excluir"
                                OnClick="Excluir_Click" CausesValidation="false"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </form>
    </div>
</body>
</html>
