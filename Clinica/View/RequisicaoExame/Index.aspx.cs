﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clinica.View.RequisicaoExame
{
    public partial class Index : System.Web.UI.Page
    {
        Clinica.Controllers.RequisicaoExame requisicao_exames_controller;

        protected void Page_Load(object sender, EventArgs e)
        {
            popularRequisicaoExames();
        }

        private void popularRequisicaoExames()
        {
            requisicao_exames_controller = Clinica.Controllers.RequisicaoExame.getInstance();
            RequisicaoExamesList.DataSource = requisicao_exames_controller.requisicoesExames;
            RequisicaoExamesList.AutoGenerateColumns = false;
            RequisicaoExamesList.DataBind();
        }

        protected void Buscar_Click(object sender, EventArgs e)
        {
        }

        protected void RequisicaoExamesList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Clinica.Model.RequisicaoExame requisicao_exame = (Clinica.Model.RequisicaoExame)e.Row.DataItem;

                HyperLink editar = e.Row.Cells[6].Controls[0] as HyperLink;
                editar.NavigateUrl = "~/View/RequisicaoExame/Edit.aspx?id=" + requisicao_exame.id;
            }
        }

        protected void Excluir_Click(object sender, EventArgs e)
        {
            requisicao_exames_controller = Clinica.Controllers.RequisicaoExame.getInstance();
            GridViewRow row = (GridViewRow) ((LinkButton) sender).NamingContainer;
            int id = Convert.ToInt32(RequisicaoExamesList.DataKeys[row.RowIndex]["id"]);
            Clinica.Model.RequisicaoExame requisicao_exame_selecionado = requisicao_exames_controller.search(id);
            requisicao_exames_controller.requisicoesExames.Remove(requisicao_exame_selecionado);
            RequisicaoExamesList.DataSource = requisicao_exames_controller.requisicoesExames;
            RequisicaoExamesList.DataBind();
        }
    }
}