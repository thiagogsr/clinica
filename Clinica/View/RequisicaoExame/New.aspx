﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="New.aspx.cs" Inherits="Clinica.View.RequisicaoExame.New" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Cadastrar Requisição de Exame</title>
    <link href="../../Assets/Stylesheet/Reset.css" rel="stylesheet" />
    <link href="../../Assets/Stylesheet/App.css" rel="stylesheet" />
</head>
<body>
    <div class="wrap">
        <h2 class="title">Cadastrar Requisição de Exame</h2>
        <%
            if (Session["flash_message"] != "") {
                Response.Write("<p class='flash-message'>" + Session["flash_message"] + "</p>");
                Session.Remove("flash_message");
            }
        %>
        <a href="Index.aspx" class="btn-link" title="Listar todas as requisições de exames">Listar todas</a>
        <form id="CadastrarRequisicaoExame" method="post" runat="server">
            <div class="container">
                <div class="half">
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="PacienteLabel" runat="server" Text="Paciente"></asp:Label>
                        <asp:DropDownList CssClass="form-control" ID="Paciente" runat="server">
                            <asp:ListItem Value="">Selecione</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator CssClass="error-message" runat="server" ControlToValidate="Paciente" ErrorMessage="Campo obrigatório"></asp:RequiredFieldValidator>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="ExameLabel" runat="server" Text="Exame"></asp:Label>
                        <asp:DropDownList CssClass="form-control" ID="Exame" runat="server">
                            <asp:ListItem Value="">Selecione</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator CssClass="error-message" runat="server" ControlToValidate="Exame" ErrorMessage="Campo obrigatório"></asp:RequiredFieldValidator>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="DataLabel" runat="server" Text="Data"></asp:Label>
                        <asp:TextBox ID="Data" CssClass="form-control data" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator CssClass="error-message" runat="server" ControlToValidate="Data" ErrorMessage="Campo obrigatório"></asp:RequiredFieldValidator>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="TipoLabel" runat="server" Text="Tipo"></asp:Label>
                        <div class="form-checkbox">
                            <div class="checkbox-item">
                                <asp:RadioButton ID="TipoConvenio" runat="server" GroupName="Tipo" Text="Convenio" />
                            </div>
                            <div class="checkbox-item">
                                <asp:RadioButton ID="TipoParticular" runat="server" GroupName="Tipo" Text="Particular" />
                            </div>
                        </div>
                    </div>
                    <div class="field show-particular" style="display: none">
                        <asp:Label CssClass="form-label" ID="ValorLabel" runat="server" Text="Valor"></asp:Label>
                        <asp:TextBox ID="Valor" CssClass="form-control moeda" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="field">
                <asp:Button CssClass="btn" ID="Criar" runat="server" Text="Salvar" UseSubmitBehavior="true" OnClick="Criar_Click" />
            </div>
        </form>
    </div>

    <script src="../../Assets/Javascript/jquery-1.11.2.min.js"></script>
    <script src="../../Assets/Javascript/jquery_maskedinput_min.js"></script>
    <script src="../../Assets/Javascript/jquery.price_format.2.0.min.js"></script>
    <script src="../../Assets/Javascript/App.js"></script>
    <script src="../../Assets/Javascript/RequisicaoExame.js"></script>
</body>
</html>