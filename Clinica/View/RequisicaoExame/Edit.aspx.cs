﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clinica.View.RequisicaoExame
{
    public partial class Edit : System.Web.UI.Page
    {
        Clinica.Controllers.RequisicaoExame requisicao_exames_controller;

        protected void Page_Load(object sender, EventArgs e)
        {
            requisicao_exames_controller = Clinica.Controllers.RequisicaoExame.getInstance();

            if (!IsPostBack)
            {
                popularCombos();

                String id = Request.QueryString["id"];
                if (id != null)
                {
                    Int32 requisicaoExameId = Int32.Parse(id);
                    Clinica.Model.RequisicaoExame requisicaoExame = requisicao_exames_controller.search(requisicaoExameId);
                    if (requisicaoExame == null)
                    {
                        Session["flash_message"] = "Requisição de Exame com ID " + requisicaoExameId + " não encontrada";
                        Response.Redirect("Index.aspx");
                    }
                    else
                    {
                        RequisicaoExameID.Value = requisicaoExame.id.ToString();
                        Paciente.Text = requisicaoExame.paciente.cpf;
                        Exame.Text = requisicaoExame.exame.id.ToString();
                        Data.Text = requisicaoExame.data.ToString();
                        TipoConvenio.Checked = requisicaoExame.tipo == "CONVENIO";
                        TipoParticular.Checked = requisicaoExame.tipo == "PARTICULAR";
                        if (TipoParticular.Checked)
                        {
                            Valor.Text = requisicaoExame.valor.ToString();
                        }
                    }
                }
                else
                {
                    Response.Redirect("Index.aspx");
                }
            }
        }

        protected void Atualizar_Click(object sender, EventArgs e)
        {
            Int32 requisicaoExameId = Int32.Parse(RequisicaoExameID.Value);
            Boolean saved = requisicao_exames_controller.update(requisicaoExameId, Paciente.Text, Exame.Text, Data.Text, TipoConvenio.Checked, Valor.Text);
            if (saved)
            {
                Session["flash_message"] = "Requisição de Exame atualizada com sucesso";
                Response.Redirect("Index.aspx");
            }
            else
            {
                Session["flash_message"] = "O convênio do paciente não permite a realização deste exame.";
                popularCombos();
            }
        }

        private void popularCombos()
        {
            Clinica.Helpers.Paciente.popular(Paciente);
            Clinica.Helpers.Exame.popular(Exame);
        }
    }
}