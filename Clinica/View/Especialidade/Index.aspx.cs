﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clinica.View.Especialidade
{
    public partial class Index : System.Web.UI.Page
    {
        Clinica.Controllers.Especialidade especialidades_controller;

        protected void Page_Load(object sender, EventArgs e)
        {
            popularEspecialidades();
        }

        private void popularEspecialidades()
        {
            especialidades_controller = Clinica.Controllers.Especialidade.getInstance();
            EspecialidadesList.DataSource = especialidades_controller.especialidades;
            EspecialidadesList.AutoGenerateColumns = false;
            EspecialidadesList.DataBind();
        }

        protected void Buscar_Click(object sender, EventArgs e)
        {
            especialidades_controller = Clinica.Controllers.Especialidade.getInstance();
            List<Clinica.Model.Especialidade> especialidades = especialidades_controller.search(Titulo.Text, Descricao.Text, CondicaoE.Checked);
            if (especialidades.Count == 0)
            {
                Session["flash_message"] = "Nenhuma especialidade foi encontrada com esses parâmetros";
            }
            EspecialidadesList.DataSource = especialidades;
            EspecialidadesList.DataBind();
        }

        protected void EspecialidadesList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Clinica.Model.Especialidade especialidade = (Clinica.Model.Especialidade)e.Row.DataItem;

                HyperLink editar = e.Row.Cells[2].Controls[0] as HyperLink;
                editar.NavigateUrl = "~/View/Especialidade/Edit.aspx?id=" + especialidade.id;
            }
        }

        protected void Excluir_Click(object sender, EventArgs e)
        {
            especialidades_controller = Clinica.Controllers.Especialidade.getInstance();
            GridViewRow row = (GridViewRow)((LinkButton)sender).NamingContainer;
            int id = Convert.ToInt32(EspecialidadesList.DataKeys[row.RowIndex]["id"]);
            Clinica.Model.Especialidade especialidade_selecionada = especialidades_controller.search(id);

            Int32 quantidade_medicos = Clinica.Controllers.Medico.getInstance().medicos_by_especialidade(especialidade_selecionada);
            if (quantidade_medicos == 0)
            {
                especialidades_controller.especialidades.Remove(especialidade_selecionada);
                EspecialidadesList.DataSource = especialidades_controller.especialidades;
                EspecialidadesList.DataBind();
            }
            else
            {
                Session["flash_message"] = "Existe(m) " + quantidade_medicos + " médico(s) associado(s) a esta especialidade e por isso a exclusão não pode ser realizada.";
            }
        }
    }
}