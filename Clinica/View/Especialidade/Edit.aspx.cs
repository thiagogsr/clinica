﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clinica.View.Especialidade
{
    public partial class Edit : System.Web.UI.Page
    {
        Clinica.Controllers.Especialidade especialidades_controller;

        protected void Page_Load(object sender, EventArgs e)
        {
            especialidades_controller = Clinica.Controllers.Especialidade.getInstance();

            if (!IsPostBack)
            {
                String id = Request.QueryString["id"];
                if (id != null)
                {
                    Int32 especialidade_id = Int32.Parse(id);
                    Clinica.Model.Especialidade especialidade = especialidades_controller.search(especialidade_id);
                    if (especialidade == null)
                    {
                        Session["flash_message"] = "Especialidade com ID " + id + " não encontrada";
                        Response.Redirect("Index.aspx");
                    }
                    else
                    {
                        EspecialidadeID.Value = especialidade.id.ToString();
                        Titulo.Text = especialidade.titulo;
                        Descricao.Text = especialidade.descricao;
                    }
                }
                else
                {
                    Response.Redirect("Index.aspx");
                }
            }
        }

        protected void Atualizar_Click(object sender, EventArgs e)
        {
            Int32 especialidade_id = Int32.Parse(EspecialidadeID.Value);
            Boolean saved = especialidades_controller.update(especialidade_id, Titulo.Text, Descricao.Text);
            if (saved)
            {
                Session["flash_message"] = "Especialidade atualizada com sucesso";
                Response.Redirect("Index.aspx");
            }
            else
            {
                Session["flash_message"] = "Já existe uma especialidade cadastrada com esse título e essa descrição.";
            }
        }
    }
}