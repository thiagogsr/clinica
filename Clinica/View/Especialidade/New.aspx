﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="New.aspx.cs" Inherits="Clinica.View.Especialidade.New" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Cadastrar Especialidade</title>
    <link href="../../Assets/Stylesheet/Reset.css" rel="stylesheet" />
    <link href="../../Assets/Stylesheet/App.css" rel="stylesheet" />
</head>
<body>
    <div class="wrap">
        <h2 class="title">Cadastrar Especialidade</h2>
        <%
            if (Session["flash_message"] != "") {
                Response.Write("<p class='flash-message'>" + Session["flash_message"] + "</p>");
                Session.Remove("flash_message");
            }
        %>
        <a href="Index.aspx" class="btn-link" title="Listar todos os especialidades">Listar todos</a>
        <form id="CadastrarEspecialidade" method="post" runat="server">
            <div class="container">
                <div class="half">
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="TituloLabel" runat="server" Text="Título"></asp:Label>
                        <asp:TextBox ID="Titulo" CssClass="form-control" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator CssClass="error-message" runat="server" ControlToValidate="Titulo" ErrorMessage="Campo obrigatório"></asp:RequiredFieldValidator>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="DescricaoLabel" runat="server" Text="Descrição"></asp:Label>
                        <asp:TextBox ID="Descricao" TextMode="MultiLine" CssClass="form-control" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator CssClass="error-message" runat="server" ControlToValidate="Descricao" ErrorMessage="Campo obrigatório"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="field">
                <asp:Button CssClass="btn" ID="Criar" runat="server" Text="Salvar" UseSubmitBehavior="true" OnClick="Criar_Click" />
            </div>
        </form>
    </div>

    <script src="../../Assets/Javascript/jquery-1.11.2.min.js"></script>
    <script src="../../Assets/Javascript/jquery_maskedinput_min.js"></script>
    <script src="../../Assets/Javascript/App.js"></script>
</body>
</html>

