﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Clinica.View.Especialidade.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Especialidades</title>
    <link href="../../Assets/Stylesheet/Reset.css" rel="stylesheet" />
    <link href="../../Assets/Stylesheet/App.css" rel="stylesheet" />
</head>
<body>
    <div class="wrap">
        <h2 class="title">Especialidades</h2>
        <%
            if (Session["flash_message"] != "") {
                Response.Write("<p class='flash-message'>" + Session["flash_message"] + "</p>");
                Session.Remove("flash_message");
            }
        %>
        <a href="/" title="Início" class="btn-link">Início</a>
        <a href="New.aspx" title="Cadastrar Especialidade" class="btn-link">Cadastrar Especialidade</a>
        <form id="BuscarEspecialidade" method="get" runat="server">
            <div class="container">
                <div class="half">
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="TituloLabel" runat="server" Text="Título"></asp:Label>
                        <asp:TextBox ID="Titulo" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="field">
                        <div class="form-checkbox">
                            <div class="checkbox-item">
                                <asp:RadioButton ID="CondicaoE" runat="server" GroupName="Condicao" Text="E" Checked="true" />
                            </div>
                            <div class="checkbox-item">
                                <asp:RadioButton ID="CondicaoOu" runat="server" GroupName="Condicao" Text="OU" />
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="DescricaoLabel" runat="server" Text="Descrição"></asp:Label>
                        <asp:TextBox ID="Descricao" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="half">
                    <div class="field">
                        <asp:Button CssClass="btn" ID="Buscar" runat="server" Text="Buscar" UseSubmitBehavior="true" OnClick="Buscar_Click" />
                    </div>
                </div>
            </div>

            <asp:GridView ID="EspecialidadesList" runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None"
                BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Horizontal" Width="100%"
                OnRowDataBound="EspecialidadesList_RowDataBound" DataKeyNames="id">
                <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                <SortedDescendingHeaderStyle BackColor="#242121" />
                <Columns>
                    <asp:BoundField HeaderText="ID" DataField="id" />
                    <asp:BoundField HeaderText="Título" DataField="titulo" />
                    <asp:HyperLinkField Text="Editar" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="Excluir" runat="server" Text="Excluir"
                                OnClick="Excluir_Click" CausesValidation="false"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </form>
    </div>
</body>
</html>
