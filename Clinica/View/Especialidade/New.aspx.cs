﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clinica.View.Especialidade
{
    public partial class New : System.Web.UI.Page
    {
        Clinica.Controllers.Especialidade especialidades_controller;

        protected void Page_Load(object sender, EventArgs e)
        {
            especialidades_controller = Clinica.Controllers.Especialidade.getInstance();
        }

        protected void Criar_Click(object sender, EventArgs e)
        {
            Boolean created = especialidades_controller.create(Titulo.Text, Descricao.Text);
            if (created)
            {
                Session["flash_message"] = "Especialidade cadastrada com sucesso";
                clearFields();
            }
            else
            {
                Session["flash_message"] = "Já existe uma especialidade cadastrada com esse título e essa descrição";
            }
        }

        private void clearFields()
        {
            Titulo.Text = "";
            Descricao.Text = "";
        }
    }
}