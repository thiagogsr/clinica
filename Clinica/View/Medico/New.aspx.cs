﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clinica.View.Medico
{
    public partial class New : System.Web.UI.Page
    {
        Clinica.Controllers.Medico medicos_controller;

        protected void Page_Load(object sender, EventArgs e)
        {
            medicos_controller = Clinica.Controllers.Medico.getInstance();
            if (!IsPostBack)
            {
                popularCombos();
            }
        }

        protected void Criar_Click(object sender, EventArgs e)
        {
            Boolean created = medicos_controller.create(Nome.Text, Endereco.Text, Cidade.Text, Estado.Text, CPF.Text, Telefone.Text, CRM.Text, Atendimentos.Text, Especialidade.Text);
            if (created)
            {
                Session["flash_message"] = "Médico cadastrado com sucesso";
                clearFields();
            }
            else
            {
                Session["flash_message"] = "Já existe um médico cadastrado com o CRM " + CRM.Text;
                popularCombos();
            }
        }

        private void clearFields()
        {
            Nome.Text = "";
            Endereco.Text = "";
            Cidade.Text = "";
            Estado.Text = "";
            CPF.Text = "";
            Telefone.Text = "";
            CRM.Text = "";
            Atendimentos.Text = "";
            Especialidade.Text = "";
        }

        private void popularCombos()
        {
            Clinica.Helpers.Estado.popular(Estado);
            Clinica.Helpers.Especialidade.popular(Especialidade);
        }
    }
}