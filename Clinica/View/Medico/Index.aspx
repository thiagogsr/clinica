﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Clinica.View.Medico.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Médicos</title>
    <link href="../../Assets/Stylesheet/Reset.css" rel="stylesheet" />
    <link href="../../Assets/Stylesheet/App.css" rel="stylesheet" />
</head>
<body>
    <div class="wrap">
        <h2 class="title">Médicos</h2>
        <%
            if (Session["flash_message"] != "") {
                Response.Write("<p class='flash-message'>" + Session["flash_message"] + "</p>");
                Session.Remove("flash_message");
            }
        %>
        <a href="/" title="Início" class="btn-link">Início</a>
        <a href="New.aspx" title="Cadastrar Médico" class="btn-link">Cadastrar Médico</a>
        <form id="BuscarMedico" method="get" runat="server">
            <div class="container">
                <div class="half">
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="CRMLabel" runat="server" Text="CRM"></asp:Label>
                        <asp:TextBox ID="CRM" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="EspecialidadeLabel" runat="server" Text="Especialidade"></asp:Label>
                        <asp:DropDownList CssClass="form-control" ID="Especialidade" runat="server">
                            <asp:ListItem Value="">Selecione</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="half">
                    <div class="field">
                        <asp:Button CssClass="btn" ID="Buscar" runat="server" Text="Buscar" UseSubmitBehavior="true" OnClick="Buscar_Click" />
                    </div>
                </div>
            </div>

            <asp:GridView ID="MedicosList" runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None"
                BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Horizontal" Width="100%"
                OnRowDataBound="MedicosList_RowDataBound" DataKeyNames="crm">
                <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                <SortedDescendingHeaderStyle BackColor="#242121" />
                <Columns>
                    <asp:BoundField HeaderText="Nome" DataField="nome" />
                    <asp:BoundField HeaderText="CRM" DataField="crm" />
                    <asp:BoundField HeaderText="Telefone" DataField="telefone" />
                    <asp:BoundField HeaderText="Especialidade" DataField="especialidade.titulo" />
                    <asp:HyperLinkField Text="Editar" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="Excluir" runat="server" Text="Excluir"
                                OnClick="Excluir_Click" CausesValidation="false"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </form>
    </div>
</body>
</html>
