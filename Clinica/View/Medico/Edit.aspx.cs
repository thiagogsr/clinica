﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clinica.View.Medico
{
    public partial class Edit : System.Web.UI.Page
    {
        Clinica.Controllers.Medico medicos_controller;

        protected void Page_Load(object sender, EventArgs e)
        {
            medicos_controller = Clinica.Controllers.Medico.getInstance();
            popularCombos();

            if (!IsPostBack)
            {
                String crm = Request.QueryString["crm"];
                if (crm != null)
                {
                    Clinica.Model.Medico medico = medicos_controller.search(crm);
                    if (medico == null)
                    {
                        Session["flash_message"] = "Médico com CRM " + crm + " não encontrado";
                        Response.Redirect("Index.aspx");
                    }
                    else
                    {
                        CRMDisabled.Text = medico.crm;
                        CRM.Value = medico.crm;
                        Nome.Text = medico.nome;
                        Endereco.Text = medico.endereco;
                        Telefone.Text = medico.telefone;
                        CPF.Text = medico.cpf;
                        Cidade.Text = medico.cidade;
                        Estado.Text = medico.estado;
                        Atendimentos.Text = medico.atendimentos.ToString();
                        Especialidade.Text = medico.especialidade.id.ToString();
                    }
                }
                else
                {
                    Response.Redirect("Index.aspx");
                }
            }
        }

        protected void Atualizar_Click(object sender, EventArgs e)
        {
            Boolean saved = medicos_controller.update(CRM.Value, Nome.Text, Endereco.Text, Cidade.Text, Estado.Text, CPF.Text, Telefone.Text, Atendimentos.Text, Especialidade.Text);
            if (saved)
            {
                Session["flash_message"] = "Médico atualizado com sucesso";
                Response.Redirect("Index.aspx");
            }
            else
            {
                Session["flash_message"] = "Falha ao atualizar o Médico. Tente novamente.";
                popularCombos();
            }
        }

        private void popularCombos()
        {
            Clinica.Helpers.Estado.popular(Estado);
            Clinica.Helpers.Especialidade.popular(Especialidade);
        }
    }
}