﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="New.aspx.cs" Inherits="Clinica.View.Medico.New" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Cadastrar Médico</title>
    <link href="../../Assets/Stylesheet/Reset.css" rel="stylesheet" />
    <link href="../../Assets/Stylesheet/App.css" rel="stylesheet" />
</head>
<body>
    <div class="wrap">
        <h2 class="title">Cadastrar Médico</h2>
        <%
            if (Session["flash_message"] != "") {
                Response.Write("<p class='flash-message'>" + Session["flash_message"] + "</p>");
                Session.Remove("flash_message");
            }
        %>
        <a href="Index.aspx" class="btn-link" title="Listar todos os médicos">Listar todos</a>
        <form id="CadastrarMedico" method="post" runat="server">
            <div class="container">
                <div class="half">
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="NomeLabel" runat="server" Text="Nome"></asp:Label>
                        <asp:TextBox ID="Nome" CssClass="form-control" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator CssClass="error-message" runat="server" ControlToValidate="Nome" ErrorMessage="Campo obrigatório"></asp:RequiredFieldValidator>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="EnderecoLabel" runat="server" Text="Endereço"></asp:Label>
                        <asp:TextBox ID="Endereco" CssClass="form-control" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" CssClass="error-message" ControlToValidate="Endereco" ErrorMessage="Campo obrigatório"></asp:RequiredFieldValidator>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="CPFLabel" runat="server" Text="CPF"></asp:Label>
                        <asp:TextBox ID="CPF" CssClass="form-control cpf" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator CssClass="error-message" runat="server" ControlToValidate="CPF" ErrorMessage="Campo obrigatório"></asp:RequiredFieldValidator>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="TelefoneLabel" runat="server" Text="Telefone"></asp:Label>
                        <asp:TextBox ID="Telefone" CssClass="form-control telefone" runat="server"></asp:TextBox>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="CRMLabel" runat="server" Text="CRM"></asp:Label>
                        <asp:TextBox ID="CRM" CssClass="form-control" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator CssClass="error-message" runat="server" ControlToValidate="CRM" ErrorMessage="Campo obrigatório"></asp:RequiredFieldValidator>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="AtendimentosLabel" runat="server" Text="Atendimentos por Turno"></asp:Label>
                        <asp:DropDownList CssClass="form-control" ID="Atendimentos" runat="server">
                            <asp:ListItem Selected="True" Value="">Selecione</asp:ListItem>
                            <asp:ListItem>1</asp:ListItem>
                            <asp:ListItem>2</asp:ListItem>
                            <asp:ListItem>3</asp:ListItem>
                            <asp:ListItem>4</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator CssClass="error-message" runat="server" ControlToValidate="Atendimentos" ErrorMessage="Campo obrigatório"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="half">
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="CidadeLabel" Text="Cidade" runat="server" />
                        <asp:TextBox ID="Cidade" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="EstadoLabel" runat="server" Text="Estado"></asp:Label>
                        <asp:DropDownList CssClass="form-control" ID="Estado" runat="server">
                            <asp:ListItem Value="">Selecione</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="EspecialidadeLabel" runat="server" Text="Especialidade"></asp:Label>
                        <asp:DropDownList CssClass="form-control" ID="Especialidade" runat="server">
                            <asp:ListItem Value="">Selecione</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator CssClass="error-message" runat="server" ControlToValidate="Especialidade" ErrorMessage="Campo obrigatório"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="field">
                <asp:Button CssClass="btn" ID="Criar" runat="server" Text="Salvar" UseSubmitBehavior="true" OnClick="Criar_Click" />
            </div>
        </form>
    </div>

    <script src="../../Assets/Javascript/jquery-1.11.2.min.js"></script>
    <script src="../../Assets/Javascript/jquery_maskedinput_min.js"></script>
    <script src="../../Assets/Javascript/App.js"></script>
</body>
</html>
