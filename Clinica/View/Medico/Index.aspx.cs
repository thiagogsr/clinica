﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clinica.View.Medico
{
    public partial class Index : System.Web.UI.Page
    {
        Clinica.Controllers.Medico medicos_controller;

        protected void Page_Load(object sender, EventArgs e)
        {
            popularMedicos();

            if (!IsPostBack)
            {
                popularCombos();
            }
        }

        private void popularMedicos()
        {
            medicos_controller = Clinica.Controllers.Medico.getInstance();
            MedicosList.DataSource = medicos_controller.medicos;
            MedicosList.AutoGenerateColumns = false;
            MedicosList.DataBind();
        }

        protected void Buscar_Click(object sender, EventArgs e)
        {
            String especialidade = Especialidade.Text;
            medicos_controller = Clinica.Controllers.Medico.getInstance();
            List<Clinica.Model.Medico> medicos = medicos_controller.search(CRM.Text, especialidade);
            
            if (medicos.Count == 0)
            {
                Session["flash_message"] = "Nenhum médico foi encontrado com esses parâmetros";
            }

            MedicosList.DataSource = medicos;
            MedicosList.DataBind();

            Especialidade.Text = especialidade;
        }

        protected void MedicosList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Clinica.Model.Medico medico = (Clinica.Model.Medico) e.Row.DataItem;

                HyperLink editar = e.Row.Cells[4].Controls[0] as HyperLink;
                editar.NavigateUrl = "~/View/Medico/Edit.aspx?crm=" + medico.crm;
            }
        }

        protected void Excluir_Click(object sender, EventArgs e)
        {
            medicos_controller = Clinica.Controllers.Medico.getInstance();
            GridViewRow row = (GridViewRow)((LinkButton)sender).NamingContainer;
            String crm = MedicosList.DataKeys[row.RowIndex]["crm"].ToString();
            Clinica.Model.Medico medico_selecionado = medicos_controller.search(crm);
            medicos_controller.medicos.Remove(medico_selecionado);
            MedicosList.DataSource = medicos_controller.medicos;
            MedicosList.DataBind();
        }

        private void popularCombos()
        {
            Clinica.Helpers.Especialidade.popular(Especialidade);
        }
    }
}