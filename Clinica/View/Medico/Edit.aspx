﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="Clinica.View.Medico.Edit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Editar Médico</title>
    <link href="../../Assets/Stylesheet/Reset.css" rel="stylesheet" />
    <link href="../../Assets/Stylesheet/App.css" rel="stylesheet" />
</head>
<body>
    <div class="wrap">
        <h2 class="title">Editar Médico</h2>
        <%
            if (Session["flash_message"] != "") {
                Response.Write("<p class='flash-message'>" + Session["flash_message"] + "</p>");
                Session.Remove("flash_message");
            }
        %>
        <a href="Index.aspx" title="Cancelar" class="btn-link">Cancelar</a>
        <form id="EditarMedico" method="post" runat="server">
            <div class="container">
                <div class="half">
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="CRMLabel" runat="server" Text="CRM"></asp:Label>
                        <asp:TextBox ID="CRMDisabled" CssClass="form-control" runat="server" Enabled="False"></asp:TextBox>
                        <asp:HiddenField ID="CRM" runat="server" />
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="NomeLabel" runat="server" Text="Nome"></asp:Label>
                        <asp:TextBox ID="Nome" CssClass="form-control" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator CssClass="error-message" runat="server" ControlToValidate="Nome" ErrorMessage="Campo obrigatório"></asp:RequiredFieldValidator>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="EnderecoLabel" runat="server" Text="Endereço"></asp:Label>
                        <asp:TextBox ID="Endereco" CssClass="form-control" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" CssClass="error-message" ControlToValidate="Endereco" ErrorMessage="Campo obrigatório"></asp:RequiredFieldValidator>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="CPFLabel" runat="server" Text="CPF"></asp:Label>
                        <asp:TextBox ID="CPF" CssClass="form-control" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator CssClass="error-message cpf" runat="server" ControlToValidate="CPF" ErrorMessage="Campo obrigatório"></asp:RequiredFieldValidator>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="TelefoneLabel" runat="server" Text="Telefone"></asp:Label>
                        <asp:TextBox ID="Telefone" CssClass="form-control telefone" runat="server"></asp:TextBox>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="AtendimentosLabel" runat="server" Text="Atendimentos por Turno"></asp:Label>
                        <asp:DropDownList CssClass="form-control" ID="Atendimentos" runat="server">
                            <asp:ListItem Selected="True" Value="">Selecione</asp:ListItem>
                            <asp:ListItem>1</asp:ListItem>
                            <asp:ListItem>2</asp:ListItem>
                            <asp:ListItem>3</asp:ListItem>
                            <asp:ListItem>4</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator CssClass="error-message" runat="server" ControlToValidate="Atendimentos" ErrorMessage="Campo obrigatório"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="half">
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="CidadeLabel" Text="Cidade" runat="server" />
                        <asp:TextBox ID="Cidade" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="EstadoLabel" runat="server" Text="Estado"></asp:Label>
                        <asp:DropDownList CssClass="form-control" ID="Estado" runat="server">
                            <asp:ListItem Value="">Selecione</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="EspecialidadeLabel" runat="server" Text="Especialidade"></asp:Label>
                        <asp:DropDownList CssClass="form-control" ID="Especialidade" runat="server">
                            <asp:ListItem Value="">Selecione</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator CssClass="error-message" runat="server" ControlToValidate="Especialidade" ErrorMessage="Campo obrigatório"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="field">
                <asp:Button CssClass="btn" ID="Atualizar" runat="server" Text="Atualizar" UseSubmitBehavior="true" OnClick="Atualizar_Click" />
            </div>
        </form>
    </div>

    <script src="../../Assets/Javascript/jquery-1.11.2.min.js"></script>
    <script src="../../Assets/Javascript/jquery_maskedinput_min.js"></script>
    <script src="../../Assets/Javascript/App.js"></script>
</body>
</html>
