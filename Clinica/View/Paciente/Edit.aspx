﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="Clinica.View.Paciente.Edit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Editar Paciente</title>
    <link href="../../Assets/Stylesheet/Reset.css" rel="stylesheet" />
    <link href="../../Assets/Stylesheet/App.css" rel="stylesheet" />
</head>
<body>
    <div class="wrap">
        <h2 class="title">Editar Paciente</h2>
        <%
            if (Session["flash_message"] != "") {
                Response.Write("<p class='flash-message'>" + Session["flash_message"] + "</p>");
                Session.Remove("flash_message");
            }
        %>
        <a href="Index.aspx" title="Cancelar" class="btn-link">Cancelar</a>
        <form id="EditarPaciente" method="post" runat="server">
            <div class="container">
                <div class="half">
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="CPFLabel" runat="server" Text="CPF"></asp:Label>
                        <asp:TextBox ID="CPFDisabled" CssClass="form-control cpf" runat="server" Enabled="false"></asp:TextBox>
                        <asp:HiddenField ID="CPF" runat="server" />
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="NomeLabel" runat="server" Text="Nome"></asp:Label>
                        <asp:TextBox ID="Nome" CssClass="form-control" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator CssClass="error-message" runat="server" ControlToValidate="Nome" ErrorMessage="Campo obrigatório"></asp:RequiredFieldValidator>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="EnderecoLabel" runat="server" Text="Endereço"></asp:Label>
                        <asp:TextBox ID="Endereco" CssClass="form-control" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator CssClass="error-message" runat="server" ControlToValidate="Endereco" ErrorMessage="Campo obrigatório"></asp:RequiredFieldValidator>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="TelefoneLabel" runat="server" Text="Telefone"></asp:Label>
                        <asp:TextBox ID="Telefone" CssClass="form-control telefone" runat="server"></asp:TextBox>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="AniversarioLabel" runat="server" Text="Data de Aniversário"></asp:Label>
                        <asp:TextBox ID="Aniversario" CssClass="form-control data" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="half">
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="CidadeLabel" Text="Cidade" runat="server" />
                        <asp:TextBox ID="Cidade" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="EstadoLabel" runat="server" Text="Estado"></asp:Label>
                        <asp:DropDownList CssClass="form-control" ID="Estado" runat="server">
                            <asp:ListItem Value="">Selecione</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="SexoLabel" runat="server" Text="Sexo"></asp:Label>
                        <div class="form-checkbox">
                            <div class="checkbox-item">
                                <asp:RadioButton ID="SexoMasculino" runat="server" GroupName="Sexo" Text="Masculino" />
                            </div>
                            <div class="checkbox-item">
                                <asp:RadioButton ID="SexoFeminino" runat="server" GroupName="Sexo" Text="Feminino" />
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="ConvenioLabel" runat="server" Text="Convênio"></asp:Label>
                        <asp:DropDownList CssClass="form-control" ID="Convenio" runat="server">
                            <asp:ListItem Value="">Selecione</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="field">
                <asp:Button CssClass="btn" ID="Atualizar" runat="server" Text="Atualizar" OnClick="Atualizar_Click" UseSubmitBehavior="true" />
            </div>
        </form>
    </div>

    <script src="../../Assets/Javascript/jquery-1.11.2.min.js"></script>
    <script src="../../Assets/Javascript/jquery_maskedinput_min.js"></script>
    <script src="../../Assets/Javascript/App.js"></script>
</body>
</html>
