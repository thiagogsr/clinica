﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Clinica.View.Paciente.Index" %>

<!DOCTYPE html>

<head runat="server">
    <title>Pacientes</title>
    <link href="../../Assets/Stylesheet/Reset.css" rel="stylesheet" />
    <link href="../../Assets/Stylesheet/App.css" rel="stylesheet" />
</head>
<body>
    <div class="wrap">
        <h2 class="title">Pacientes</h2>
        <%
            if (Session["flash_message"] != "") {
                Response.Write("<p class='flash-message'>" + Session["flash_message"] + "</p>");
                Session.Remove("flash_message");
            }
        %>
        <a href="/" title="Início" class="btn-link">Início</a>
        <a href="New.aspx" title="Cadastrar Paciente" class="btn-link">Cadastrar Paciente</a>
        <form id="BuscarPaciente" method="get" runat="server">
            <div class="container">
                <div class="half">
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="CPFLabel" runat="server" Text="CPF"></asp:Label>
                        <asp:TextBox ID="CPF" CssClass="form-control" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator CssClass="error-message" runat="server" ControlToValidate="CPF" ErrorMessage="Campo obrigatório"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="half">
                    <div class="field">
                        <asp:Button CssClass="btn" ID="Buscar" runat="server" Text="Buscar" UseSubmitBehavior="true" OnClick="Buscar_Click" />
                    </div>
                </div>
            </div>

            <asp:GridView ID="PacientesList" runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None"
                BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Horizontal" Width="100%"
                OnRowDataBound="PacientesList_RowDataBound" DataKeyNames="cpf">
                <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                <SortedDescendingHeaderStyle BackColor="#242121" />
                <Columns>
                    <asp:BoundField HeaderText="Nome" DataField="nome" />
                    <asp:BoundField HeaderText="CPF" DataField="cpf" />
                    <asp:BoundField HeaderText="Telefone" DataField="telefone" />
                    <asp:HyperLinkField Text="Editar" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="Excluir" runat="server" Text="Excluir"
                                OnClick="Excluir_Click" CausesValidation="false"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </form>
    </div>
</body>
</html>