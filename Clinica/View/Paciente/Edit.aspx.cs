﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clinica.View.Paciente
{
    public partial class Edit : System.Web.UI.Page
    {
        Clinica.Controllers.Paciente pacientes_controller;

        protected void Page_Load(object sender, EventArgs e)
        {
            pacientes_controller = Clinica.Controllers.Paciente.getInstance();

            if (!IsPostBack)
            {
                Clinica.Helpers.Estado.popular(Estado);
                Clinica.Helpers.Convenio.popular(Convenio);

                String cpf = Request.QueryString["cpf"];
                if (cpf != null)
                {
                    Clinica.Model.Paciente paciente = pacientes_controller.search(cpf);
                    if (paciente == null)
                    {
                        Session["flash_message"] = "Paciente com CPF " + cpf + " não encontrado";
                        Response.Redirect("Index.aspx");
                    }
                    else
                    {
                        CPFDisabled.Text = paciente.cpf;
                        CPF.Value = paciente.cpf;
                        Nome.Text = paciente.nome;
                        Endereco.Text = paciente.endereco;
                        Telefone.Text = paciente.telefone;
                        Aniversario.Text = paciente.aniversario.ToString();
                        if (paciente.sexo.Equals("F"))
                        {
                            SexoFeminino.Checked = true;
                        }
                        else
                        {
                            SexoMasculino.Checked = true;
                        }
                        Cidade.Text = paciente.cidade;
                        Estado.Text = paciente.estado;
                        if (paciente.convenio != null)
                        {
                            Convenio.SelectedValue = paciente.convenio.id.ToString();
                        }
                    }
                }
                else
                {
                    Response.Redirect("Index.aspx");
                }
            }
        }

        protected void Atualizar_Click(object sender, EventArgs e)
        {
            Boolean saved = pacientes_controller.update(CPF.Value, Nome.Text, Endereco.Text, Cidade.Text, Estado.Text, Telefone.Text, Aniversario.Text, SexoFeminino.Checked, SexoMasculino.Checked, Convenio.SelectedValue);
            if (saved)
            {
                Session["flash_message"] = "Paciente atualizado com sucesso";
                Response.Redirect("Index.aspx");
            }
            else
            {
                Session["flash_message"] = "Falha ao atualizar o Paciente. Tente novamente.";
            }
        }
    }
}