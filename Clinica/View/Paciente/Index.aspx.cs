﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clinica.View.Paciente
{
    public partial class Index : System.Web.UI.Page
    {
        Clinica.Controllers.Paciente pacientes_controller;

        protected void Page_Load(object sender, EventArgs e)
        {
            popularPacientes();
        }

        private void popularPacientes()
        {
            pacientes_controller = Clinica.Controllers.Paciente.getInstance();
            PacientesList.DataSource = pacientes_controller.pacientes;
            PacientesList.AutoGenerateColumns = false;
            PacientesList.DataBind();
        }

        protected void Buscar_Click(object sender, EventArgs e)
        {
            String cpf = CPF.Text;
            pacientes_controller = Clinica.Controllers.Paciente.getInstance();
            Clinica.Model.Paciente paciente = pacientes_controller.search(cpf);
            if (paciente == null)
            {
                Session["flash_message"] = "Paciente com CPF " + cpf + " não encontrado";
            }
            else
            {
                Response.Redirect("/View/Paciente/Edit.aspx?cpf=" + cpf);
            }
        }

        protected void PacientesList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Clinica.Model.Paciente paciente = (Clinica.Model.Paciente) e.Row.DataItem;

                HyperLink editar = e.Row.Cells[3].Controls[0] as HyperLink;
                editar.NavigateUrl = "~/View/Paciente/Edit.aspx?cpf=" + paciente.cpf;
            }
        }

        protected void Excluir_Click(object sender, EventArgs e)
        {
            pacientes_controller = Clinica.Controllers.Paciente.getInstance();
            GridViewRow row = (GridViewRow) ((LinkButton) sender).NamingContainer;
            String cpf = PacientesList.DataKeys[row.RowIndex]["cpf"].ToString();
            Clinica.Model.Paciente paciente_selecionado = pacientes_controller.search(cpf);
            pacientes_controller.pacientes.Remove(paciente_selecionado);
            PacientesList.DataSource = pacientes_controller.pacientes;
            PacientesList.DataBind();
        }
    }
}