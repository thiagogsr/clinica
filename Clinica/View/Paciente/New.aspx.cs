﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clinica.View.Paciente
{
    public partial class New : System.Web.UI.Page
    {
        Clinica.Controllers.Paciente pacientes_controller;

        protected void Page_Load(object sender, EventArgs e)
        {
            pacientes_controller = Clinica.Controllers.Paciente.getInstance();

            if (!IsPostBack)
            {
                Clinica.Helpers.Estado.popular(Estado);
                Clinica.Helpers.Convenio.popular(Convenio);
            }
        }

        protected void Criar_Click(object sender, EventArgs e)
        {
            Boolean created = pacientes_controller.create(Nome.Text, Endereco.Text, Cidade.Text, Estado.Text, CPF.Text, Telefone.Text, Aniversario.Text, SexoFeminino.Checked, SexoMasculino.Checked, Convenio.SelectedValue);
            if (created)
            {
                Session["flash_message"] = "Paciente cadastrado com sucesso";
                clearFields();
            }
            else
            {
                Session["flash_message"] = "Já existe um paciente cadastrado com o CPF " + CPF.Text;
                Clinica.Helpers.Estado.popular(Estado);
                Clinica.Helpers.Convenio.popular(Convenio);
            }
        }

        private void clearFields()
        {
            Nome.Text = "";
            Endereco.Text = "";
            Cidade.Text = "";
            Estado.Text = "";
            CPF.Text = "";
            Aniversario.Text = "";
            Telefone.Text = "";
            Convenio.Text = "";
        }
    }
}