﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="Clinica.View.Consulta.Edit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Editar Consulta</title>
    <link href="../../Assets/Stylesheet/Reset.css" rel="stylesheet" />
    <link href="../../Assets/Stylesheet/App.css" rel="stylesheet" />
</head>
<body>
    <div class="wrap">
        <h2 class="title">Editar Consulta</h2>
        <%
            if (Session["flash_message"] != "") {
                Response.Write("<p class='flash-message'>" + Session["flash_message"] + "</p>");
                Session.Remove("flash_message");
            }
        %>
        <a href="Index.aspx" title="Cancelar" class="btn-link">Cancelar</a>
        <form id="EditarConsulta" method="post" runat="server">
            <asp:HiddenField ID="ConsultaID" runat="server" />
            <div class="container">
                <div class="half">
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="PacienteLabel" runat="server" Text="Paciente"></asp:Label>
                        <asp:DropDownList CssClass="form-control" ID="Paciente" runat="server">
                            <asp:ListItem Value="">Selecione</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator CssClass="error-message" runat="server" ControlToValidate="Paciente" ErrorMessage="Campo obrigatório"></asp:RequiredFieldValidator>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="MedicoLabel" runat="server" Text="Médico"></asp:Label>
                        <asp:DropDownList CssClass="form-control" ID="Medico" runat="server">
                            <asp:ListItem Value="">Selecione</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator CssClass="error-message" runat="server" ControlToValidate="Medico" ErrorMessage="Campo obrigatório"></asp:RequiredFieldValidator>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="ConvenioLabel" runat="server" Text="Convênio"></asp:Label>
                        <asp:DropDownList CssClass="form-control" ID="Convenio" runat="server">
                            <asp:ListItem Value="">Selecione</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator CssClass="error-message" runat="server" ControlToValidate="Convenio" ErrorMessage="Campo obrigatório"></asp:RequiredFieldValidator>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="DataLabel" runat="server" Text="Data"></asp:Label>
                        <asp:TextBox ID="Data" CssClass="form-control data" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator CssClass="error-message" runat="server" ControlToValidate="Data" ErrorMessage="Campo obrigatório"></asp:RequiredFieldValidator>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="TurnoLabel" runat="server" Text="Turno"></asp:Label>
                        <asp:DropDownList CssClass="form-control" ID="Turno" runat="server">
                            <asp:ListItem Value="">Selecione</asp:ListItem>
                            <asp:ListItem Value="Manha">Manhã</asp:ListItem>
                            <asp:ListItem Value="Tarde">Tarde</asp:ListItem>
                            <asp:ListItem Value="Noite">Noite</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator CssClass="error-message" runat="server" ControlToValidate="Turno" ErrorMessage="Campo obrigatório"></asp:RequiredFieldValidator>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="SituacaoLabel" runat="server" Text="Situação"></asp:Label>
                        <div class="form-checkbox">
                            <asp:RadioButtonList runat="server" ValidationGroup="Situacao" ID="Situacao"></asp:RadioButtonList>
                        </div>
                        <asp:RequiredFieldValidator CssClass="error-message" runat="server" ControlToValidate="Situacao" ErrorMessage="Campo obrigatório"></asp:RequiredFieldValidator>
                    </div>
                    <div class="field show-realizada" style="display: none">
                        <asp:Label CssClass="form-label" ID="MedicamentosLabel" runat="server" Text="Medicamentos"></asp:Label>
                        <asp:TextBox ID="Medicamentos" CssClass="form-control" TextMode="MultiLine" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="field">
                <asp:Button CssClass="btn" ID="Atualizar" runat="server" Text="Atualizar" UseSubmitBehavior="true" OnClick="Atualizar_Click" />
            </div>
        </form>
    </div>

    <script src="../../Assets/Javascript/jquery-1.11.2.min.js"></script>
    <script src="../../Assets/Javascript/jquery_maskedinput_min.js"></script>
    <script src="../../Assets/Javascript/App.js"></script>
    <script src="../../Assets/Javascript/Consulta.js"></script>
</body>
</html>
