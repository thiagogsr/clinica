﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clinica.View.Consulta
{
    public partial class Index : System.Web.UI.Page
    {
        Clinica.Controllers.Consulta consultas_controller;

        protected void Page_Load(object sender, EventArgs e)
        {
            popularConsultas();

            if (!IsPostBack)
            {
                popularCombos();
            }
        }

        private void popularConsultas()
        {
            consultas_controller = Clinica.Controllers.Consulta.getInstance();
            ConsultasList.DataSource = consultas_controller.consultas;
            ConsultasList.AutoGenerateColumns = false;
            ConsultasList.DataBind();
        }

        protected void Buscar_Click(object sender, EventArgs e)
        {
            consultas_controller = Clinica.Controllers.Consulta.getInstance();
            String paciente = Paciente.Text;
            String medico = Medico.Text;
            String convenio = Convenio.Text;
            String situacao = Situacao.Text;
            List<Clinica.Model.Consulta> consultas = consultas_controller.search(paciente,
                medico, convenio, Data.Text, Turno.Text, situacao);

            if (consultas.Count == 0)
            {
                Session["flash_message"] = "Nenhuma consulta foi encontrada com esses parâmetros.";
            }
            ConsultasList.DataSource = consultas;
            ConsultasList.DataBind();

            Paciente.Text = paciente;
            Medico.Text = medico;
            Convenio.Text = convenio;
            if (!String.IsNullOrEmpty(situacao))
            {
                Situacao.Text = situacao;
            }
        }

        protected void ConsultasList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Clinica.Model.Consulta consulta = (Clinica.Model.Consulta) e.Row.DataItem;

                HyperLink editar = e.Row.Cells[7].Controls[0] as HyperLink;
                editar.NavigateUrl = "~/View/Consulta/Edit.aspx?id=" + consulta.id;
            }
        }

        protected void Excluir_Click(object sender, EventArgs e)
        {
            consultas_controller = Clinica.Controllers.Consulta.getInstance();
            GridViewRow row = (GridViewRow)((LinkButton)sender).NamingContainer;
            Int32 consultaId = Convert.ToInt32(ConsultasList.DataKeys[row.RowIndex]["id"]);
            Clinica.Model.Consulta consulta_selecionada = consultas_controller.search(consultaId);
            consultas_controller.consultas.Remove(consulta_selecionada);
            ConsultasList.DataSource = consultas_controller.consultas;
            ConsultasList.DataBind();
        }

        private void popularCombos()
        {
            Clinica.Helpers.Paciente.popular(Paciente);
            Clinica.Helpers.Medico.popular(Medico);
            Clinica.Helpers.Convenio.popular(Convenio);
            Clinica.Helpers.SituacaoConsulta.popular(Situacao);
        }
    }
}