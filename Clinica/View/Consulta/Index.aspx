﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Clinica.View.Consulta.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Consultas</title>
    <link href="../../Assets/Stylesheet/Reset.css" rel="stylesheet" />
    <link href="../../Assets/Stylesheet/App.css" rel="stylesheet" />
</head>
<body>
    <div class="wrap">
        <h2 class="title">Consultas</h2>
        <%
            if (Session["flash_message"] != "") {
                Response.Write("<p class='flash-message'>" + Session["flash_message"] + "</p>");
                Session.Remove("flash_message");
            }
        %>
        <a href="/" title="Início" class="btn-link">Início</a>
        <a href="New.aspx" title="Cadastrar Consulta" class="btn-link">Cadastrar Consulta</a>
        <form id="BuscarConsulta" runat="server">
            <div class="container">
                <div class="form-horizontal">
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="PacienteLabel" runat="server" Text="Paciente"></asp:Label>
                        <asp:DropDownList CssClass="form-control" ID="Paciente" runat="server">
                            <asp:ListItem Value="">Selecione</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="MedicoLabel" runat="server" Text="Médico"></asp:Label>
                        <asp:DropDownList CssClass="form-control" ID="Medico" runat="server">
                            <asp:ListItem Value="">Selecione</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="ConvenioLabel" runat="server" Text="Convênio"></asp:Label>
                        <asp:DropDownList CssClass="form-control" ID="Convenio" runat="server">
                            <asp:ListItem Value="">Selecione</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="DataLabel" runat="server" Text="Data"></asp:Label>
                        <asp:TextBox ID="Data" CssClass="form-control data" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form-horizontal">
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="TurnoLabel" runat="server" Text="Turno"></asp:Label>
                        <asp:DropDownList CssClass="form-control" ID="Turno" runat="server">
                            <asp:ListItem Value="">Selecione</asp:ListItem>
                            <asp:ListItem Value="Manha">Manhã</asp:ListItem>
                            <asp:ListItem Value="Tarde">Tarde</asp:ListItem>
                            <asp:ListItem Value="Noite">Noite</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="SituacaoLabel" runat="server" Text="Situação"></asp:Label>
                        <div class="form-checkbox">
                            <asp:RadioButtonList runat="server" ValidationGroup="Situacao" ID="Situacao"></asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="field">
                        <asp:Button CssClass="btn" ID="Buscar" runat="server" Text="Buscar" UseSubmitBehavior="true" OnClick="Buscar_Click" />
                    </div>
                </div>
            </div>

            <asp:GridView ID="ConsultasList" runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None"
                BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Horizontal" Width="100%"
                OnRowDataBound="ConsultasList_RowDataBound" DataKeyNames="id">
                <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                <SortedDescendingHeaderStyle BackColor="#242121" />
                <Columns>
                    <asp:BoundField HeaderText="ID" DataField="id" />
                    <asp:BoundField HeaderText="Paciente" DataField="paciente.nome" />
                    <asp:BoundField HeaderText="Médico" DataField="medico.nome" />
                    <asp:BoundField HeaderText="Convênio" DataField="convenio.titulo" />
                    <asp:BoundField HeaderText="Data" DataField="data" />
                    <asp:BoundField HeaderText="Turno" DataField="turno" />
                    <asp:BoundField HeaderText="Situação" DataField="situacao" />
                    <asp:HyperLinkField Text="Editar" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="Excluir" runat="server" Text="Excluir"
                                OnClick="Excluir_Click" CausesValidation="false"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </form>
    </div>

    <script src="../../Assets/Javascript/jquery-1.11.2.min.js"></script>
    <script src="../../Assets/Javascript/jquery_maskedinput_min.js"></script>
    <script src="../../Assets/Javascript/App.js"></script>
</body>
</html>
