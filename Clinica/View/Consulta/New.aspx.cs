﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clinica.View.Consulta
{
    public partial class New : System.Web.UI.Page
    {
        Clinica.Controllers.Consulta consultas_controller;

        protected void Page_Load(object sender, EventArgs e)
        {
            consultas_controller = Clinica.Controllers.Consulta.getInstance();
            if (!IsPostBack)
            {
                popularCombos();
            }
        }

        protected void Criar_Click(object sender, EventArgs e)
        {
            String turno = Turno.Text;
            String data = Data.Text;
            Boolean created = consultas_controller.create(Paciente.Text, Convenio.Text, Medico.Text, data, turno);
            if (created)
            {
                Session["flash_message"] = "Consulta cadastrada com sucesso";
                clearFields();
            }
            else
            {
                Session["flash_message"] = "Esse médico não pode mais atender no turno da " + turno + " do dia " + data + ".";
                popularCombos();
            }
        }

        private void clearFields()
        {
            Paciente.Text = "";
            Convenio.Text = "";
            Medico.Text = "";
            Data.Text = "";
            Turno.Text = "";
        }

        private void popularCombos()
        {
            Clinica.Helpers.Paciente.popular(Paciente);
            Clinica.Helpers.Medico.popular(Medico);
            Clinica.Helpers.Convenio.popular(Convenio);
        }
    }
}