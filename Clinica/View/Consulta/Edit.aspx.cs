﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clinica.View.Consulta
{
    public partial class Edit : System.Web.UI.Page
    {
        Clinica.Controllers.Consulta consultas_controller;

        protected void Page_Load(object sender, EventArgs e)
        {
            consultas_controller = Clinica.Controllers.Consulta.getInstance();

            if (!IsPostBack)
            {
                popularCombos();

                String id = Request.QueryString["id"];
                if (id != null)
                {
                    Int32 consulta_id = Int32.Parse(id);
                    Clinica.Model.Consulta consulta = consultas_controller.search(consulta_id);
                    if (consulta == null)
                    {
                        Session["flash_message"] = "Consulta com ID " + id + " não encontrada";
                        Response.Redirect("Index.aspx");
                    }
                    else
                    {
                        ConsultaID.Value = consulta.id.ToString();
                        Paciente.Text = consulta.paciente.cpf;
                        Medico.Text = consulta.medico.crm;
                        Convenio.Text = consulta.convenio.id.ToString();
                        Data.Text = consulta.data.ToString();
                        Turno.Text = consulta.turno;
                        Situacao.Text = consulta.situacao;
                        Medicamentos.Text = consulta.medicamentos;
                    }
                }
                else
                {
                    Response.Redirect("Index.aspx");
                }
            }
        }

        protected void Atualizar_Click(object sender, EventArgs e)
        {
            String data = Data.Text;
            String turno = Turno.Text;
            Boolean saved = consultas_controller.update(ConsultaID.Value, Paciente.Text, Convenio.Text, Medico.Text, data, turno, Situacao.Text, Medicamentos.Text);
            if (saved)
            {
                Session["flash_message"] = "Consulta atualizada com sucesso";
                Response.Redirect("Index.aspx");
            }
            else
            {
                Session["flash_message"] = "Esse médico não pode mais atender no turno da " + turno + " do dia " + data + ".";
                popularCombos();
            }
        }

        private void popularCombos()
        {
            Clinica.Helpers.Paciente.popular(Paciente);
            Clinica.Helpers.Medico.popular(Medico);
            Clinica.Helpers.Convenio.popular(Convenio);
            Clinica.Helpers.SituacaoConsulta.popular(Situacao);
        }
    }
}