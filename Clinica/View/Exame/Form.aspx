﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Form.aspx.cs" Inherits="Clinica.View.Exame.Form" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Manutenção Exame</title>
    <link href="../../Assets/Stylesheet/Reset.css" rel="stylesheet" />
    <link href="../../Assets/Stylesheet/App.css" rel="stylesheet" />
</head>
<body>
    <div class="wrap">
        <h2 class="title">Manutenção Exame</h2>
        <%
            if (Session["flash_message"] != "") {
                Response.Write("<p class='flash-message'>" + Session["flash_message"] + "</p>");
                Session.Remove("flash_message");
            }
        %>
        <a href="Index.aspx" class="btn-link" title="Listar todos os exames">Listar todos</a>
        <form id="ManterExame" method="post" runat="server">
            <asp:HiddenField ID="ExameID" runat="server" />
            <div class="container">
                <div class="half">
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="TituloLabel" runat="server" Text="Titulo"></asp:Label>
                        <asp:TextBox ID="Titulo" CssClass="form-control" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator CssClass="error-message" runat="server" ControlToValidate="Titulo" ErrorMessage="Campo obrigatório"></asp:RequiredFieldValidator>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="CodigoLabel" runat="server" Text="Codigo"></asp:Label>
                        <asp:TextBox ID="Codigo" CssClass="form-control" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator CssClass="error-message" runat="server" ControlToValidate="Codigo" ErrorMessage="Campo obrigatório"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="half">
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="ObservacoesLabel" runat="server" Text="Observacoes"></asp:Label>
                        <asp:TextBox ID="Observacoes" CssClass="form-control" TextMode="MultiLine" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator CssClass="error-message" runat="server" ControlToValidate="Observacoes" ErrorMessage="Campo obrigatório"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="half">
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="ConvenioLabel" runat="server" Text="Convênio"></asp:Label>
                        <asp:DropDownList CssClass="form-control" ID="Convenio" runat="server">
                            <asp:ListItem Value="">Selecione</asp:ListItem>
                        </asp:DropDownList>
                        <asp:Button CssClass="btn" ID="AdicionarConvenio" runat="server" Text="Adicionar" CausesValidation="false" OnClick="AdicionarConvenio_Click" />
                    </div>
                </div>
                <div class="half">
                    <asp:GridView ID="ConveniosList" runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" AutoGenerateColumns="false"
                        BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Horizontal" Width="100%" DataKeyNames="id">
                        <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                        <Columns>
                            <asp:BoundField HeaderText="Sigla" DataField="sigla" />
                            <asp:BoundField HeaderText="Convênio" DataField="titulo" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="Remover" runat="server" Text="Remover"
                                        OnClick="Remover_Click" CausesValidation="false"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <div class="field">
                <asp:Button CssClass="btn" ID="Salvar" runat="server" Text="Salvar" UseSubmitBehavior="true" OnClick="Salvar_Click" />
            </div>
        </form>
    </div>
</body>
</html>

