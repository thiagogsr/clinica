﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clinica.View.Exame
{
    public partial class Form : System.Web.UI.Page
    {
        Clinica.Controllers.Exame exames_controller;
        List<Clinica.Model.Convenio> convenios;

        protected void Page_Load(object sender, EventArgs e)
        {
            exames_controller = Clinica.Controllers.Exame.getInstance();
            convenios = (List<Clinica.Model.Convenio>) Session["convenios"];

            if (!IsPostBack)
            {
                convenios = new List<Clinica.Model.Convenio>();
                Session["convenios"] = convenios;
                popularCombos();

                String id = Request.QueryString["id"];
                if (id != null)
                {
                    Int32 exame_id = Int32.Parse(id);
                    Clinica.Model.Exame exame = exames_controller.search(exame_id);
                    if (exame == null)
                    {
                        Session["flash_message"] = "Exame com ID " + id + " não encontrado";
                        Response.Redirect("Index.aspx");
                    }
                    else
                    {
                        ExameID.Value = exame.id.ToString();
                        Titulo.Text = exame.titulo;
                        Codigo.Text = exame.codigo;
                        Observacoes.Text = exame.observacoes;
                        convenios.AddRange(exame.convenios);
                        ConveniosList.DataSource = convenios;
                        ConveniosList.DataBind();
                        Session["convenios"] = convenios;
                    }
                }
            }
        }

        protected void Salvar_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ExameID.Value))
            {
                Int32 exame_id = Int32.Parse(ExameID.Value);
                Boolean updated = exames_controller.update(exame_id, Titulo.Text, Codigo.Text, Observacoes.Text, convenios);
                if (updated)
                {
                    Session["flash_message"] = "Exame atualizado com sucesso";
                    Response.Redirect("Index.aspx");
                }
                else
                {
                    Session["flash_message"] = "Falha ao atualizar o Exame. Tente novamente.";
                }
            }
            else
            {
                String codigo = Codigo.Text;
                Boolean created = exames_controller.create(Titulo.Text, codigo, Observacoes.Text, convenios);
                if (created)
                {
                    Session["flash_message"] = "Exame cadastrado com sucesso";
                    clearFields();
                }
                else
                {
                    Session["flash_message"] = "Já existe um exame cadastrado com o código " + codigo;
                    popularCombos();
                }
            }
        }

        private void clearFields()
        {
            Titulo.Text = "";
            Codigo.Text = "";
            Observacoes.Text = "";
            Convenio.Text = "";
            convenios = new List<Clinica.Model.Convenio>();
            ConveniosList.DataSource = convenios;
            ConveniosList.DataBind();
            Session["convenios"] = convenios;
        }

        private void popularCombos()
        {
            Clinica.Helpers.Convenio.popular(Convenio);
        }

        protected void AdicionarConvenio_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Convenio.Text))
            {
                Int32 convenio_id = Int32.Parse(Convenio.Text);
                Clinica.Model.Convenio convenio = Clinica.Controllers.Convenio.getInstance().search(convenio_id);

                if (!convenios.Contains(convenio))
                {
                    convenios.Add(convenio);
                    ConveniosList.DataSource = convenios;
                    ConveniosList.DataBind();
                    Session["convenios"] = convenios;
                }
            }
        }

        protected void Remover_Click(object sender, EventArgs e)
        {
            GridViewRow row = (GridViewRow)((LinkButton)sender).NamingContainer;
            String convenioId = ConveniosList.DataKeys[row.RowIndex]["id"].ToString();
            Clinica.Model.Convenio convenio_selecionado = Clinica.Helpers.Convenio.search(convenioId);

            convenios.Remove(convenio_selecionado);
            ConveniosList.DataSource = convenios;
            ConveniosList.DataBind();
            Session["convenios"] = convenios;
        }
    }
}