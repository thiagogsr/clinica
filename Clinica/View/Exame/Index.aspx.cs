﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clinica.View.Exame
{
    public partial class Index : System.Web.UI.Page
    {
        Clinica.Controllers.Exame exames_controller;

        protected void Page_Load(object sender, EventArgs e)
        {
            popularExames();
        }

        private void popularExames()
        {
            exames_controller = Clinica.Controllers.Exame.getInstance();
            ExamesList.DataSource = exames_controller.exames;
            ExamesList.AutoGenerateColumns = false;
            ExamesList.DataBind();
        }

        protected void Buscar_Click(object sender, EventArgs e)
        {
            exames_controller = Clinica.Controllers.Exame.getInstance();
            String codigo = Codigo.Text;
            Clinica.Model.Exame exame = exames_controller.search(codigo);
            if (exame == null)
            {
                Session["flash_message"] = "Exame com código " + codigo + " não encontrado";
            }
            else
            {
                Response.Redirect("/View/Exame/Form.aspx?id=" + exame.id);
            }
        }

        protected void ExamesList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Clinica.Model.Exame exame = (Clinica.Model.Exame) e.Row.DataItem;

                HyperLink editar = e.Row.Cells[3].Controls[0] as HyperLink;
                editar.NavigateUrl = "~/View/Exame/Form.aspx?id=" + exame.id;
            }
        }

        protected void Excluir_Click(object sender, EventArgs e)
        {
            exames_controller = Clinica.Controllers.Exame.getInstance();
            GridViewRow row = (GridViewRow) ((LinkButton) sender).NamingContainer;
            int id = Convert.ToInt32(ExamesList.DataKeys[row.RowIndex]["id"]);
            Clinica.Model.Exame exame_selecionado = exames_controller.search(id);
            exames_controller.exames.Remove(exame_selecionado);
            ExamesList.DataSource = exames_controller.exames;
            ExamesList.DataBind();
        }
    }
}