﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clinica.View.Seed
{
    public partial class Index : System.Web.UI.Page
    {
        Clinica.Controllers.Convenio convenios_controller;
        Clinica.Controllers.Medico medicos_controller;
        Clinica.Controllers.Paciente pacientes_controller;
        Clinica.Controllers.Exame exames_controller;
        Clinica.Controllers.Consulta consultas_controller;
        Clinica.Controllers.RequisicaoExame requisicoes_exames_controller;
        Clinica.Controllers.Especialidade especialidades_controller;

        protected void Page_Load(object sender, EventArgs e)
        {
            consultas_controller = Clinica.Controllers.Consulta.getInstance();
            pacientes_controller = Clinica.Controllers.Paciente.getInstance();
            medicos_controller = Clinica.Controllers.Medico.getInstance();
            convenios_controller = Clinica.Controllers.Convenio.getInstance();
            exames_controller = Clinica.Controllers.Exame.getInstance();
            requisicoes_exames_controller = Clinica.Controllers.RequisicaoExame.getInstance();
            especialidades_controller = Clinica.Controllers.Especialidade.getInstance();

            carregarConvenios();
            carregarEspecialidades();
            carregarMedicos();
            carregarPacientes();
            carregarExames();
            carregarConsultas();
            carregarRequisicoesExames();
        }

        private void carregarConvenios()
        {
            convenios_controller.create("GEAP", "GEA", "7991159336");
            convenios_controller.create("Unimed", "UNI", "7999628793");
            convenios_controller.create("HapVida", "HAP", "7999999999");

            ConveniosList.DataSource = convenios_controller.convenios;
            ConveniosList.DataTextField = "titulo";
            ConveniosList.DataBind();
        }
        
        private void carregarMedicos()
        {
            medicos_controller.create("Dr. Abacaxi", "Rua da Horta", "Hortolândia", "SP", "123.123.123-01", "7999999999", "123", "2", especialidades_controller.especialidades[0].id.ToString());
            medicos_controller.create("Dra. Pêra", "Rua da Horta", "Hortolândia", "RJ", "123.123.123-02", "7999999999", "124", "1", especialidades_controller.especialidades[1].id.ToString());
            medicos_controller.create("Dr. Mamão", "Rua da Horta", "Hortolândia", "GO", "123.123.123-03", "7999999999", "125", "4", especialidades_controller.especialidades[2].id.ToString());

            MedicosList.DataSource = medicos_controller.medicos;
            MedicosList.DataTextField = "nome";
            MedicosList.DataBind();
        }

        private void carregarPacientes()
        {
            pacientes_controller.create("Firmino", "Rua A", "Aracaju", "SE", "222.333.444-01", "7912341234", "01/02/1990", false, true, "1");
            pacientes_controller.create("Joaquina", "Rua B", "Salvador", "BA", "222.333.555-01", "7112341234", "01/02/1992", true, false, "2");
            pacientes_controller.create("Procópio", "Rua C", "Maceió", "AL", "222.333.666-01", "8212341234", "01/02/1995", false, true, "3");

            PacientesList.DataSource = pacientes_controller.pacientes;
            PacientesList.DataTextField = "nome";
            PacientesList.DataBind();
        }

        private void carregarExames()
        {
            List<Clinica.Model.Convenio> convenios = new List<Clinica.Model.Convenio>();
            convenios.AddRange(convenios_controller.convenios);

            exames_controller.create("Endoscopia", "END", "Jejum", convenios);
            exames_controller.create("Raio X", "RAI", "Sem metal", convenios);
            exames_controller.create("Ultrasom", "ULT", "Só a tarde", convenios);

            ExamesList.DataSource = exames_controller.exames;
            ExamesList.DataTextField = "titulo";
            ExamesList.DataBind();
        }

        private void carregarConsultas()
        {
            consultas_controller.create(pacientes_controller.pacientes[0].cpf, convenios_controller.convenios[0].id.ToString(), medicos_controller.medicos[0].crm, "15/08/2015", "Manha");
            consultas_controller.create(pacientes_controller.pacientes[1].cpf, convenios_controller.convenios[1].id.ToString(), medicos_controller.medicos[1].crm, "02/03/2016", "Tarde");
            consultas_controller.create(pacientes_controller.pacientes[2].cpf, convenios_controller.convenios[2].id.ToString(), medicos_controller.medicos[2].crm, "19/03/2014", "Noite");

            ConsultasList.DataSource = consultas_controller.consultas;
            ConsultasList.DataTextField = "data";
            ConsultasList.DataBind();
        }

        private void carregarRequisicoesExames()
        {
            requisicoes_exames_controller.create(pacientes_controller.pacientes[0].cpf, exames_controller.exames[0].id.ToString(), "15/08/2015", false, "100,78");
            requisicoes_exames_controller.create(pacientes_controller.pacientes[1].cpf, exames_controller.exames[1].id.ToString(), "05/11/2015", true, "");
            requisicoes_exames_controller.create(pacientes_controller.pacientes[2].cpf, exames_controller.exames[2].id.ToString(), "01/05/2015", true, "");

            RequisicoesExamesList.DataSource = requisicoes_exames_controller.requisicoesExames;
            RequisicoesExamesList.DataTextField = "data";
            RequisicoesExamesList.DataBind();
        }

        private void carregarEspecialidades()
        {
            especialidades_controller.create("Ortopedia", "Cuida dos ossos");
            especialidades_controller.create("Gastrologia", "Cuida do estômago");
            especialidades_controller.create("Oftamologia", "Cuida da visão");

            EspecialidadesList.DataSource = especialidades_controller.especialidades;
            EspecialidadesList.DataTextField = "titulo";
            EspecialidadesList.DataBind();
        }
    }
}