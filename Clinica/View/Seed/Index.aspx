﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Clinica.View.Seed.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Dados carregados</title>
</head>
<body>
    <a href="/" title="Início">Início</a>
    <form id="DadosCarregados" method="get" runat="server">
        <h1>Dados carregados!</h1>
        <h2>Convênios:</h2>
        <asp:BulletedList ID="ConveniosList" runat="server"></asp:BulletedList>
        <h2>Médicos:</h2>
        <asp:BulletedList ID="MedicosList" runat="server"></asp:BulletedList>
        <h2>Pacientes:</h2>
        <asp:BulletedList ID="PacientesList" runat="server"></asp:BulletedList>
        <h2>Exames:</h2>
        <asp:BulletedList ID="ExamesList" runat="server"></asp:BulletedList>
        <h2>Consultas:</h2>
        <asp:BulletedList ID="ConsultasList" runat="server"></asp:BulletedList>
        <h2>Requisições de Exames:</h2>
        <asp:BulletedList ID="RequisicoesExamesList" runat="server"></asp:BulletedList>
        <h2>Especialidades:</h2>
        <asp:BulletedList ID="EspecialidadesList" runat="server"></asp:BulletedList>
    </form>
</body>
</html>
