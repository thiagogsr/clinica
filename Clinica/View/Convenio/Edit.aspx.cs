﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clinica.View.Convenio
{
    public partial class Edit : System.Web.UI.Page
    {
        Clinica.Controllers.Convenio convenios_controller;

        protected void Page_Load(object sender, EventArgs e)
        {
            convenios_controller = Clinica.Controllers.Convenio.getInstance();

            if (!IsPostBack)
            {
                String id = Request.QueryString["id"];
                if (id != null)
                {
                    Int32 convenio_id = Int32.Parse(id);
                    Clinica.Model.Convenio convenio = convenios_controller.search(convenio_id);
                    if (convenio == null)
                    {
                        Session["flash_message"] = "Convênio com ID " + id + " não encontrado";
                        Response.Redirect("Index.aspx");
                    }
                    else
                    {
                        ConvenioID.Value = convenio.id.ToString();
                        Titulo.Text = convenio.titulo;
                        Sigla.Text = convenio.sigla;
                        Telefone.Text = convenio.telefone;
                    }
                }
                else
                {
                    Response.Redirect("Index.aspx");
                }
            }
        }

        protected void Atualizar_Click(object sender, EventArgs e)
        {
            Int32 convenio_id = Int32.Parse(ConvenioID.Value);
            Boolean saved = convenios_controller.update(convenio_id, Titulo.Text, Sigla.Text, Telefone.Text);
            if (saved)
            {
                Session["flash_message"] = "Convênio atualizado com sucesso";
                Response.Redirect("Index.aspx");
            }
            else
            {
                Session["flash_message"] = "Falha ao atualizar o Convênio. Tente novamente.";
            }
        }
    }
}