﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clinica.View.Convenio
{
    public partial class New : System.Web.UI.Page
    {
        Clinica.Controllers.Convenio convenios_controller;

        protected void Page_Load(object sender, EventArgs e)
        {
            convenios_controller = Clinica.Controllers.Convenio.getInstance();
        }

        protected void Criar_Click(object sender, EventArgs e)
        {
            String titulo = Titulo.Text;
            Boolean created = convenios_controller.create(titulo, Sigla.Text, Telefone.Text);
            if (created)
            {
                Session["flash_message"] = "Convênio cadastrado com sucesso";
                clearFields();
            }
            else
            {
                Session["flash_message"] = "Já existe um convênio cadastrado com o título " + titulo;
            }
        }

        private void clearFields()
        {
            Titulo.Text = "";
            Sigla.Text = "";
            Telefone.Text = "";
        }
    }
}