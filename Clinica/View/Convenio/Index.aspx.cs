﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Clinica.View.Convenio
{
    public partial class Index : System.Web.UI.Page
    {
        Clinica.Controllers.Convenio convenios_controller;

        protected void Page_Load(object sender, EventArgs e)
        {
            popularConvenios();
        }

        private void popularConvenios()
        {
            convenios_controller = Clinica.Controllers.Convenio.getInstance();
            ConveniosList.DataSource = convenios_controller.convenios;
            ConveniosList.AutoGenerateColumns = false;
            ConveniosList.DataBind();
        }

        protected void Buscar_Click(object sender, EventArgs e)
        {
            convenios_controller = Clinica.Controllers.Convenio.getInstance();
            String sigla = Sigla.Text;
            Clinica.Model.Convenio convenio = convenios_controller.search(sigla);
            if (convenio == null)
            {
                Session["flash_message"] = "Convênio com sigla " + sigla + " não encontrado";
            }
            else
            {
                Response.Redirect("/View/Convenio/Edit.aspx?id=" + convenio.id);
            }
        }

        protected void ConveniosList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Clinica.Model.Convenio convenio = (Clinica.Model.Convenio)e.Row.DataItem;

                HyperLink editar = e.Row.Cells[4].Controls[0] as HyperLink;
                editar.NavigateUrl = "~/View/Convenio/Edit.aspx?id=" + convenio.id;
            }
        }

        protected void Excluir_Click(object sender, EventArgs e)
        {
            convenios_controller = Clinica.Controllers.Convenio.getInstance();
            GridViewRow row = (GridViewRow) ((LinkButton) sender).NamingContainer;
            int id = Convert.ToInt32(ConveniosList.DataKeys[row.RowIndex]["id"]);
            Clinica.Model.Convenio convenio_selecionado = convenios_controller.search(id);
            convenios_controller.convenios.Remove(convenio_selecionado);
            ConveniosList.DataSource = convenios_controller.convenios;
            ConveniosList.DataBind();
        }
    }
}