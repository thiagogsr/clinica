﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="Clinica.View.Convenio.Edit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Editar Convênio</title>
    <link href="../../Assets/Stylesheet/Reset.css" rel="stylesheet" />
    <link href="../../Assets/Stylesheet/App.css" rel="stylesheet" />
</head>
<body>
    <div class="wrap">
        <h2 class="title">Editar Convênio</h2>
        <%
            if (Session["flash_message"] != "") {
                Response.Write("<p class='flash-message'>" + Session["flash_message"] + "</p>");
                Session.Remove("flash_message");
            }
        %>
        <a href="Index.aspx" title="Cancelar" class="btn-link">Cancelar</a>
        <form id="EditarConvenio" method="post" runat="server">
            <asp:HiddenField ID="ConvenioID" runat="server" />
            <div class="container">
                <div class="half">
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="TituloLabel" runat="server" Text="Titulo"></asp:Label>
                        <asp:TextBox ID="Titulo" CssClass="form-control" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator CssClass="error-message" runat="server" ControlToValidate="Titulo" ErrorMessage="Campo obrigatório"></asp:RequiredFieldValidator>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="SiglaLabel" runat="server" Text="Sigla"></asp:Label>
                        <asp:TextBox ID="Sigla" CssClass="form-control" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator CssClass="error-message" runat="server" ControlToValidate="Sigla" ErrorMessage="Campo obrigatório"></asp:RequiredFieldValidator>
                    </div>
                    <div class="field">
                        <asp:Label CssClass="form-label" ID="TelefoneLabel" runat="server" Text="Telefone"></asp:Label>
                        <asp:TextBox ID="Telefone" CssClass="form-control telefone" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator CssClass="error-message" runat="server" ControlToValidate="Telefone" ErrorMessage="Campo obrigatório"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="field">
                <asp:Button CssClass="btn" ID="Atualizar" runat="server" Text="Atualizar" UseSubmitBehavior="true" OnClick="Atualizar_Click" />
            </div>
        </form>
    </div>

    <script src="../../Assets/Javascript/jquery-1.11.2.min.js"></script>
    <script src="../../Assets/Javascript/jquery_maskedinput_min.js"></script>
    <script src="../../Assets/Javascript/App.js"></script>
</body>
</html>
