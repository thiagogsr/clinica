﻿using Clinica.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clinica.Controllers
{
    public class Consulta
    {
        private static Consulta instance = null;
        private static Int32 id;
        public List<Clinica.Model.Consulta> consultas { private set; get; }

        private Consulta()
        {
            id = 0;
            consultas = new List<Clinica.Model.Consulta>();
        }

        public Boolean create(String paciente, String convenio, String medico, String data, String turno)
        {
            Clinica.Model.Medico obj_medico = Clinica.Helpers.Medico.search(medico);
            DateTime data_consulta = Util.parseDate(data);

            if (obj_medico.atendimentos == consultas.FindAll(ByMedico(obj_medico, data_consulta, turno)).Count)
            {
                return false;
            }

            Clinica.Model.Convenio obj_convenio = Clinica.Helpers.Convenio.search(convenio);
            Clinica.Model.Paciente obj_paciente = Clinica.Helpers.Paciente.search(paciente);

            Clinica.Model.Consulta consulta = new Clinica.Model.Consulta(++id, obj_paciente, obj_convenio, obj_medico, data_consulta, turno, "AGENDADA", null);
            consultas.Add(consulta);
            return true;
        }

        public Boolean update(String id, String paciente, String convenio, String medico, String data, String turno, String situacao, String medicamentos)
        {
            Int32 consultaId = Int32.Parse(id);
            Clinica.Model.Consulta consulta = search(consultaId);
            Clinica.Model.Medico obj_medico = Clinica.Helpers.Medico.search(medico);
            DateTime data_consulta = Util.parseDate(data);

            if (consulta == null || (consulta.medico.id != obj_medico.id && obj_medico.atendimentos == consultas.FindAll(ByMedico(obj_medico, data_consulta, turno)).Count))
            {
                return false;
            }
            if (consulta == null)
            {
                return false;
            }
            else
            {
                consulta.paciente = Clinica.Helpers.Paciente.search(paciente);
                consulta.convenio = Clinica.Helpers.Convenio.search(convenio);
                consulta.medico = obj_medico;
                consulta.data = data_consulta;
                consulta.situacao = situacao;
                consulta.medicamentos = tratarMedicamentos(situacao, medicamentos);
                consultas.Remove(consulta);
                consultas.Add(consulta);
            }
            return true;
        }

        public Clinica.Model.Consulta search(Int32 consulta_id)
        {
            return consultas.Find(consulta => consulta.id.Equals(consulta_id));
        }

        public List<Clinica.Model.Consulta> search(String paciente, String medico, String convenio, String data, String turno, String situacao)
        {
            return consultas.FindAll(consulta => (String.IsNullOrEmpty(paciente) || consulta.paciente.cpf == paciente)
                                        && (String.IsNullOrEmpty(medico) || consulta.medico.crm == medico)
                                        && (String.IsNullOrEmpty(convenio) || consulta.convenio.id == Int32.Parse(convenio))
                                        && (String.IsNullOrEmpty(data) || consulta.data == Util.parseDate(data))
                                        && (String.IsNullOrEmpty(turno) || consulta.turno == turno)
                                        && (String.IsNullOrEmpty(situacao) || consulta.situacao == situacao));
        }

        public static Consulta getInstance()
        {
            if (instance == null)
            {
                instance = new Consulta();
            }
            return instance;
        }

        private String tratarMedicamentos(String situacao, String medicamentos)
        {
            return situacao == "REALIZADA" ? medicamentos : null;
        }

        private static Predicate<Clinica.Model.Consulta> ByMedico(Clinica.Model.Medico medico, DateTime data, String turno)
        {
            return delegate(Clinica.Model.Consulta consulta)
            {
                return consulta.medico.crm == medico.crm && consulta.data == data && consulta.turno == turno;
            };
        }
    }
}