using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clinica.Controllers
{
    public class Exame
    {
        private static Exame instance = null;
        private static Int32 id;
        public List<Clinica.Model.Exame> exames { private set; get; }

        private Exame()
        {
            exames = new List<Clinica.Model.Exame>();
            id = 0;
        }

        public Boolean create(String titulo, String codigo, String observacoes, List<Clinica.Model.Convenio> convenios)
        {
            Clinica.Model.Exame found = search(codigo);
            if (found == null)
            {
                Clinica.Model.Exame exame = new Clinica.Model.Exame(++id, titulo, codigo, observacoes, convenios);
                exames.Add(exame);
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean update(Int32 id, String titulo, String codigo, String observacoes, List<Clinica.Model.Convenio> convenios)
        {
            Clinica.Model.Exame exame = search(id);
            if (exame == null)
            {
                return false;
            }
            else
            {
                exame.titulo = titulo;
                exame.codigo = codigo;
                exame.observacoes = observacoes;
                exame.convenios = convenios;
                exames.Remove(exame);
                exames.Add(exame);
            }
            return true;
        }

        public Clinica.Model.Exame search(Int32 exame_id)
        {
            return exames.Find(exame => exame.id.Equals(exame_id));
        }

        public Clinica.Model.Exame search(String codigo)
        {
            return exames.Find(exame => exame.codigo.Equals(codigo));
        }

        public static Exame getInstance()
        {
            if (instance == null)
            {
                instance = new Exame();
            }
            return instance;
        }
    }
}