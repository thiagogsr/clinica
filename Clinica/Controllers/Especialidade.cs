﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clinica.Controllers
{
    public class Especialidade
    {
        private static Especialidade instance = null;
        private static Int32 id;
        public List<Clinica.Model.Especialidade> especialidades { private set; get; }

        private Especialidade()
        {
            especialidades = new List<Clinica.Model.Especialidade>();
            id = 0;
        }

        public Boolean create(String titulo, String descricao)
        {
            if (search(titulo, descricao) == null)
            {
                Clinica.Model.Especialidade especialidade = new Clinica.Model.Especialidade(++id, titulo, descricao);
                especialidades.Add(especialidade);
                return true;
            }
            return false;
        }

        public Boolean update(Int32 id, String titulo, String descricao)
        {
            Clinica.Model.Especialidade especialidade = search(id);
            Clinica.Model.Especialidade found = search(titulo, descricao);
            if (especialidade == null || (found != null && !found.id.Equals(id)))
            {
                return false;
            }
            else
            {
                especialidade.titulo = titulo;
                especialidade.descricao = descricao;
                especialidades.Remove(especialidade);
                especialidades.Add(especialidade);
            }
            return true;
        }

        public Clinica.Model.Especialidade search(Int32 especialidadeId)
        {
            return especialidades.Find(especialidade => especialidade.id.Equals(especialidadeId));
        }

        public Clinica.Model.Especialidade search(String titulo, String descricao)
        {
            return especialidades.Find(especialidade => especialidade.titulo.Equals(titulo)
                                        && especialidade.descricao.Equals(descricao));
        }

        public List<Clinica.Model.Especialidade> search(String titulo, String descricao, Boolean and)
        {
            if (and)
            {
                return especialidades.FindAll(especialidade => (String.IsNullOrEmpty(titulo) || especialidade.titulo.Equals(titulo))
                                                && (String.IsNullOrEmpty(descricao) || especialidade.descricao.Contains(descricao)));
            }
            return especialidades.FindAll(especialidade => especialidade.titulo.Equals(titulo)
                                            || especialidade.descricao.Contains(descricao));
        }

        public static Especialidade getInstance()
        {
            if (instance == null)
            {
                instance = new Especialidade();
            }
            return instance;
        }
    }
}