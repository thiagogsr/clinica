﻿using Clinica.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Clinica.Controllers
{
    public class Paciente
    {
        private static Paciente instance = null;
        private Clinica.Controllers.Convenio convenios_controller;
        public List<Clinica.Model.Paciente> pacientes { private set; get; }

        private Paciente()
        {
            pacientes = new List<Clinica.Model.Paciente>();
            convenios_controller = Clinica.Controllers.Convenio.getInstance();
        }

        public Boolean create(String nome, String endereco, String cidade, String estado, String cpf, String telefone, String aniversario, Boolean sexo_feminino, Boolean sexo_masculino, String convenio_selected)
        {
            Clinica.Model.Paciente found = search(cpf);
            if (found == null)
            {
                Clinica.Model.Convenio convenio = searchConvenio(convenio_selected);
                String sexo = Sexo(sexo_feminino);
                Nullable<DateTime> data_aniversario = null;
                if (!String.IsNullOrEmpty(aniversario))
                {
                    data_aniversario = Util.parseDate(aniversario);
                }
                String new_cpf = Util.onlyNumbers(cpf);
                String new_telefone = Util.onlyNumbers(telefone);

                Clinica.Model.Paciente paciente = new Clinica.Model.Paciente(nome, endereco, cidade, estado, new_cpf, new_telefone, data_aniversario, sexo, convenio);
                pacientes.Add(paciente);
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean update(String cpf, String nome, String endereco, String cidade, String estado, String telefone, String aniversario, Boolean sexo_feminino, Boolean sexo_masculino, String convenio_selected)
        {
            Clinica.Model.Paciente paciente = search(cpf);
            if (paciente == null)
            {
                return false;
            }
            else
            {
                paciente.nome = nome;
                paciente.endereco = endereco;
                paciente.cidade = cidade;
                paciente.estado = estado;
                paciente.cpf = Util.onlyNumbers(cpf);
                paciente.telefone = Util.onlyNumbers(telefone);
                paciente.aniversario = null;
                if (!String.IsNullOrEmpty(aniversario))
                {
                    paciente.aniversario = Util.parseDate(aniversario);
                }
                paciente.sexo = Sexo(sexo_feminino);
                paciente.convenio = searchConvenio(convenio_selected);
                pacientes.Remove(paciente);
                pacientes.Add(paciente);
            }
            return true;
        }

        public Clinica.Model.Paciente search(String CPF)
        {
            return pacientes.Find(paciente => paciente.cpf.Equals(CPF));
        }

        public static Paciente getInstance()
        {
            if (instance == null)
            {
                instance = new Paciente();
            }
            return instance;
        }

        private Clinica.Model.Convenio searchConvenio(String convenio_selected)
        {
            Clinica.Model.Convenio convenio = null;
            if (convenio_selected.Trim() != "")
            {
                Int32 convenio_id = Int32.Parse(convenio_selected);
                convenio = convenios_controller.search(convenio_id);
            }
            return convenio;
        }

        private String Sexo(Boolean sexo_feminino)
        {
            return sexo_feminino ? "F" : "M";
        }
    }
}