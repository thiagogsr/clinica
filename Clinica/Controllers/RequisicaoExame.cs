using Clinica.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clinica.Controllers
{
    public class RequisicaoExame
    {
        private static RequisicaoExame instance = null;
        private static Int32 id;
        public List<Clinica.Model.RequisicaoExame> requisicoesExames { private set; get; }

        private RequisicaoExame()
        {
            requisicoesExames = new List<Clinica.Model.RequisicaoExame>();
            id = 0;
        }

        public Boolean create(String paciente, String exame, String data, Boolean tipoConvenio, String valor)
        {
            Clinica.Model.Paciente obj_paciente = Clinica.Helpers.Paciente.search(paciente);
            Clinica.Model.Exame obj_exame = Clinica.Helpers.Exame.search(exame);

            if (!verificarConvenio(tipoConvenio, obj_exame, obj_paciente.convenio))
            {
                return false;
            }
            
            DateTime dataRequisicao = Util.parseDate(data);
            String tipo = tratarTipo(tipoConvenio);
            Decimal valorParticular = valorRequisicao(tipoConvenio, valor);

            Clinica.Model.RequisicaoExame requisicaoExame = new Clinica.Model.RequisicaoExame(++id, obj_paciente, obj_exame, dataRequisicao, tipo, valorParticular);
            requisicoesExames.Add(requisicaoExame);
            return true;
        }

        public Boolean update(Int32 id, String paciente, String exame, String data, Boolean tipoConvenio, String valor)
        {
            Clinica.Model.RequisicaoExame requisicaoExame = search(id);
            Clinica.Model.Paciente obj_paciente = Clinica.Helpers.Paciente.search(paciente);
            Clinica.Model.Exame obj_exame = Clinica.Helpers.Exame.search(exame);

            if (requisicaoExame == null || !verificarConvenio(tipoConvenio, obj_exame, obj_paciente.convenio))
            {
                return false;
            }
            else
            {
                requisicaoExame.paciente = obj_paciente;
                requisicaoExame.exame = obj_exame;
                requisicaoExame.tipo = tratarTipo(tipoConvenio);
                requisicaoExame.data = Util.parseDate(data);
                requisicaoExame.valor = valorRequisicao(tipoConvenio, valor);
                requisicoesExames.Remove(requisicaoExame);
                requisicoesExames.Add(requisicaoExame);
            }
            return true;
        }

        public Clinica.Model.RequisicaoExame search(Int32 requisicaoExameId)
        {
            return requisicoesExames.Find(requisicaoExame => requisicaoExame.id.Equals(requisicaoExameId));
        }

        private String tratarTipo(Boolean tipoConvenio)
        {
            return tipoConvenio ? "CONVENIO" : "PARTICULAR";
        }

        private Decimal valorRequisicao(Boolean tipoConvenio, String valor)
        {
            if (tipoConvenio || (!tipoConvenio && String.IsNullOrEmpty(valor)))
            {
                return 0;
            }
            return Util.parseDecimal(valor);
        }

        private Boolean verificarConvenio(Boolean tipoConvenio, Clinica.Model.Exame exame, Clinica.Model.Convenio convenio)
        {
            if (tipoConvenio && !exame.convenios.Contains(convenio))
            {
                return false;
            }
            return true;
        }

        public static RequisicaoExame getInstance()
        {
            if (instance == null)
            {
                instance = new RequisicaoExame();
            }
            return instance;
        }
    }
}