﻿using Clinica.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clinica.Controllers
{
    public class Convenio
    {
        private static Convenio instance = null;
        private static Int32 id;
        public List<Clinica.Model.Convenio> convenios { private set; get; }

        private Convenio()
        {
            convenios = new List<Clinica.Model.Convenio>();
            id = 0;
        }

        public Boolean create(String titulo, String sigla, String telefone)
        {
            Clinica.Model.Convenio found = search(titulo);
            if (found == null)
            {
                Clinica.Model.Convenio convenio = new Clinica.Model.Convenio(++id, titulo, sigla, Util.onlyNumbers(telefone));
                convenios.Add(convenio);
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean update(Int32 id, String titulo, String sigla, String telefone)
        {
            Clinica.Model.Convenio convenio = search(id);
            if (convenio == null)
            {
                return false;
            }
            else
            {
                convenio.titulo = titulo;
                convenio.sigla = sigla;
                convenio.telefone = Util.onlyNumbers(telefone);
                convenios.Remove(convenio);
                convenios.Add(convenio);
            }
            return true;
        }

        public Clinica.Model.Convenio search(Int32 convenio_id)
        {
            return convenios.Find(convenio => convenio.id.Equals(convenio_id));
        }

        public Clinica.Model.Convenio search(String sigla)
        {
            return convenios.Find(convenio => convenio.sigla.Equals(sigla));
        }

        public static Convenio getInstance()
        {
            if (instance == null)
            {
                instance = new Convenio();
            }
            return instance;
        }
    }
}