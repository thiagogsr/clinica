﻿using Clinica.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clinica.Controllers
{
    public class Medico
    {
        private static Medico instance = null;
        public List<Clinica.Model.Medico> medicos { private set; get; }

        private Medico()
        {
            medicos = new List<Clinica.Model.Medico>();
        }

        public Boolean create(String nome, String endereco, String cidade, String estado, String cpf, String telefone, String crm, String atendimentos, String especialidade)
        {
            Clinica.Model.Medico found = search(crm);
            if (found == null)
            {
                int num_atendimentos = Int32.Parse(atendimentos);
                String new_cpf = Util.onlyNumbers(cpf);
                String new_telefone = Util.onlyNumbers(telefone);
                String new_crm = Util.onlyNumbers(crm);
                Clinica.Model.Especialidade obj_especialidade = Clinica.Helpers.Especialidade.search(especialidade);

                Clinica.Model.Medico medico = new Clinica.Model.Medico(nome, endereco, cidade, estado, new_cpf, new_telefone, new_crm, num_atendimentos, obj_especialidade);
                medicos.Add(medico);
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean update(String crm, String nome, String endereco, String cidade, String estado, String cpf, String telefone, String atendimentos, String especialidade)
        {
            Clinica.Model.Medico medico = search(crm);
            if (medico == null)
            {
                return false;
            }
            else
            {
                int num_atendimentos = Int32.Parse(atendimentos);
                medico.nome = nome;
                medico.endereco = endereco;
                medico.cidade = cidade;
                medico.estado = estado;
                medico.cpf = Util.onlyNumbers(cpf);
                medico.telefone = Util.onlyNumbers(telefone);
                medico.atendimentos = num_atendimentos;
                medico.especialidade = Clinica.Helpers.Especialidade.search(especialidade);
                medicos.Remove(medico);
                medicos.Add(medico);
            }
            return true;
        }

        public Clinica.Model.Medico search(String CRM)
        {
            return medicos.Find(medico => medico.crm.Equals(CRM));
        }

        public List<Clinica.Model.Medico> search(String CRM, String especialidade)
        {
            return medicos.FindAll(medico => (String.IsNullOrEmpty(CRM) || medico.crm.Equals(CRM))
                                    && (String.IsNullOrEmpty(especialidade) || medico.especialidade.id.Equals(Int32.Parse(especialidade))));
        }

        public Int32 medicos_by_especialidade(Clinica.Model.Especialidade especialidade)
        {
            return medicos.FindAll(ByEspecialidade(especialidade)).Count;
        }

        private static Predicate<Clinica.Model.Medico> ByEspecialidade(Clinica.Model.Especialidade especialidade)
        {
            return delegate(Clinica.Model.Medico medico)
            {
                return especialidade.id == medico.especialidade.id;
            };
        }

        public static Medico getInstance()
        {
            if (instance == null)
            {
                instance = new Medico();
            }
            return instance;
        }
    }
}